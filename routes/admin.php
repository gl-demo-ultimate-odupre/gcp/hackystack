<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

Route::namespace('Admin')->group(function () {

    // Dashboard
    Route::get('/admin/dashboard', 'DashboardController@show')->name('admin.dashboard');

});
