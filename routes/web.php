<?php

use Illuminate\Support\Facades\Route;

// The routes have been been defined in separate files in the routes/ directory
// and loaded in the app/Providers/RouteServiceProvider class.
//
// routes/auth.php
// routes/user.php
// routes/admin.php
