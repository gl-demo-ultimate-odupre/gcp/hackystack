<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_roles', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->index();
            $table->string('name');
            $table->string('slug')->nullable()->index();
            $table->text('description')->nullable();
            $table->boolean('flag_is_elevated')->default(false)->nullable();
            $table->boolean('flag_is_group_member')->default(false)->nullable();
            $table->json('permissions');
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
            $table->unique(['auth_tenant_id', 'slug'], 'auth_roles_tenant_slug_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_roles');
    }
}
