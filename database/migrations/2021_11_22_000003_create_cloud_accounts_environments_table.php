<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudAccountsEnvironmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_accounts_environments', function (Blueprint $table) {
            $table->uuid('id')->index();
            $table->string('short_id', 8)->index();
            $table->uuid('auth_tenant_id')->index();
            $table->uuid('cloud_account_id')->index();
            $table->uuid('cloud_account_environment_template_id')->index('cloud_accounts_environments_index_template_id');
            $table->uuid('cloud_organization_unit_id')->index('cloud_accounts_environments_index_organization_unit_id');
            $table->uuid('cloud_provider_id')->index('cloud_accounts_environments_index_provider_id');
            $table->uuid('cloud_realm_id')->index();
            $table->json('git_meta_data')->nullable();
            $table->string('git_project_id', 20)->nullable();
            $table->string('name', 55)->nullable();
            $table->string('description', 255)->nullable();
            $table->timestamp('provisioned_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_accounts_environments');
    }
}
