<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiredAtToCloudAccountsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('expired_at', 'cloud_accounts_users')) {
            return;
        }

        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->timestamp('expired_at')->nullable()->after('expires_at');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->dropColumn('expired_at');
        });
    }
}
