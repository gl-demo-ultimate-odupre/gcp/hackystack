<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagExpiredToCloudAccountsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('flag_expired', 'cloud_accounts_users')) {
            return;
        }

        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->timestamp('flag_expired')->nullable()->after('flag_provisioned');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->dropColumn('flag_expired');
        });
    }
}
