const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Javascript
mix.js('resources/js/app.js', 'public/js')
mix.copy('node_modules/@coreui/utils/dist/coreui-utils.js', 'public/js');
mix.copy('node_modules/axios/dist/axios.min.js', 'public/js');
mix.copy('node_modules/@coreui/coreui/dist/js/coreui.bundle.min.js', 'public/js');
mix.copy('node_modules/chart.js/dist/Chart.min.js', 'public/js');
mix.copy('node_modules/@coreui/chartjs/dist/js/coreui-chartjs.bundle.js', 'public/js');

// CSS
mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/css');
mix.copy('node_modules/@coreui/chartjs/dist/css/coreui-chartjs.css', 'public/css');
mix.sass('resources/sass/bootstrap.scss', 'public/css');
mix.sass('resources/sass/coreui.scss', 'public/css');

// Fonts
mix.copy('node_modules/remixicon/fonts', 'public/fonts');
mix.copy('node_modules/typeface-inter', 'public/fonts');

// Static assets
mix.copy('resources/assets', 'public/assets');

// Source maps for debugging
mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css.map', 'public/css');
mix.copy('node_modules/@coreui/coreui/dist/js/coreui.bundle.min.js.map', 'public/js');
mix.copy('node_modules/@coreui/chartjs/dist/js/coreui-chartjs.bundle.js.map', 'public/js');
mix.copy('node_modules/@coreui/chartjs/dist/css/coreui-chartjs.css.map', 'public/css');
