<?php

namespace Tests\Unit\app\Services\V1\Vendor\Gcp;

use Tests\Fakes\CloudResourceManagerIamUserRoleServiceFake;

it('returns true if email role binding exists', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));

    $iam_email = 'serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com';
    $role_name = 'roles/compute.admin';

    $role_object = (object) [
        "role" => "roles/compute.admin",
        "members" => [
            "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
        ]
    ];

    $role_binding = [
        $role_object
    ];

    $has_role_binding = $service->checkIfEmailRoleBindingExists($role_binding, $role_name, $iam_email);
    expect($has_role_binding)->toBeTrue();
});


it('returns false if email role binding does not exists', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));

    $iam_email = 'serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com';
    $role_name = 'roles/incorrect_role.admin';

    $role_object = (object) [
        "role" => "roles/compute.admin",
        "members" => [
            "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
        ]
    ];

    $role_binding = [
        $role_object
    ];

    $has_role_binding = $service->checkIfEmailRoleBindingExists($role_binding, $role_name, $iam_email);
    expect($has_role_binding)->toBeFalse();
});

it('returns true if email role exists in role binding with multiple roles in array', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));

    $iam_email = 'serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com';
    $role_name = 'roles/compute.admin';

    $role_object = [
        (object) [
            "role" => "roles/compute.admin",
            "members" => [
                "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
            ]
        ],
        (object) [
            "role" => "roles/compute.not_admin",
            "members" => [
                'serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com'
            ]
        ]
    ];

    $has_role_binding = $service->checkIfEmailRoleBindingExists($role_object, $role_name, $iam_email);
    expect($has_role_binding)->toBeTrue();
});

it('can create the role bindings array', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));

    $role_name = 'roles/compute.admin';

    $binding_array = $service->createRoleBindingDefinition($role_name);
    expect($binding_array->role)->toBe($role_name);
});

it('can check if role binding array exists', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $role_object = [
        (object) [
            "role" => "roles/compute.admin",
            "members" => [
                "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
            ]
        ],
        (object) [
            "role" => "roles/compute.not_admin",
            "members" => [
                'serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com'
            ]
        ]
    ];

    $compute_admin_results = $service->checkIfRoleBindingArrayExists($role_object, 'roles/compute.admin');
    $compute_not_admin_results = $service->checkIfRoleBindingArrayExists($role_object, 'roles/compute.not_admin');
    expect($compute_admin_results)->toBeTrue();
    expect($compute_not_admin_results)->toBeTrue();
});

it('can initialize google cloud sdk', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $project_id = config('tests.gcp.project_id');
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);
    $api_scopes = [
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/compute'
    ];
    $file_path = $service->getJsonKeyFilePath();
    $initialized = $service->setGlamstackGoogleCloudSdk($api_scopes, $file_path, $project_id);
    expect(gettype($initialized))->toBe('object');
    expect(get_class($initialized))->toBe('Glamstack\GoogleCloud\ApiClient');
});
