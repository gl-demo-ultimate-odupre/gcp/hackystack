<?php

namespace Tests\Feature\app\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\CloudResourceManagerIamUserRoleService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\Fakes\CloudResourceManagerIamUserRoleServiceFake;
use TypeError;

it('initializes the service with a valid cloud provider and can generate an API token', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));

    expect($service)->toBeInstanceOf(CloudResourceManagerIamUserRoleServiceFake::class);
});

it('initializes the service with an invalid cloud provider', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'aws')->first();
    if (!$gcp_cloud_provider) {
        throw new Exception('No Cloud Provider with the `aws` type exists in the database.');
    }

    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
})->expectExceptionMessage('The GCP service is trying to use a cloud provider that is not the correct type.');

it('can set the project_id class variable to a numeric value', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_id');
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);


    $service->setProjectId($project_id);

    expect($service->getProjectId())->toBe($project_id);
});

it('can set the project_id class variable to an alpha-dash value', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_name');

    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);

    $service->setProjectId($project_id);

    expect($service->getProjectId())->toBe($project_id);
});

it('cannot set the project_id class variable as an array and throws a type error', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->first();
    $project_id = [config('tests.gcp.project_name')];
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);
})->expectException('TypeError');

it('can get the current IAM Policy from the Google API', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_name');
    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);
    expect($service->getIamPolicy())->toHaveProperty('bindings');
});

it('can update the IAM Policy using the Google API', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $project_id = config('tests.gcp.project_name');

    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);

    $policy = $service->getIamPolicy();

    // TODO: I do not know if this test is actually testing what we think it is

    expect($service->updateIamPolicy($policy)->object)->toHaveProperty('bindings');
});

it('can attach an IAM user to a role using the IAM Policy binding array with the Google API', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $gcp_cloud_account = \App\Models\Cloud\CloudAccount::query()
        ->where('cloud_provider_id', $gcp_cloud_provider->id)
        ->firstOrFail();

    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $gcp_cloud_account->api_meta_data['projectId']);

    $role = 'roles/serviceusage.serviceUsageViewer';
    $iam_email = config('tests.gcp.iam_user_email');

    expect($service->attach($role, $iam_email))->toBe(true);
    // TODO Add additional logic to check for existence of role and user.

    $service->detach($role, $iam_email);
});

it('can detach an IAM user from a role using the IAM Policy binding array with the Google API', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_name');

    $service = new CloudResourceManagerIamUserRoleServiceFake($gcp_cloud_provider->id, $project_id);

    $role = 'roles/serviceusage.serviceUsageViewer';
    $iam_email = config('tests.gcp.iam_user_email');

    $service->attach($role, $iam_email);

    expect($service->detach($role, $iam_email))->toBe(true);
});

it('can check if role binding array exists', function () {
})->skip('Not implemented yet');

it('can create a role binding array', function () {
})->skip('Not implemented yet');

it('can check if email role binding exists', function () {
})->skip('Not implemented yet');

it('can add email to role in policy object', function () {
})->skip('Not implemented yet');

it('can remove email from role in policy object', function () {
})->skip('Not implemented yet');
