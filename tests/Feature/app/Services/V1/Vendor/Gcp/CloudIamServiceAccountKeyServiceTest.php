<?php

namespace Tests\Feature\app\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\CloudIamServiceAccountService;
use App\Services\V1\Vendor\Gcp\CloudIamServiceAccountKeyService;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\Fakes\CloudIamServiceAccountKeyServiceFake;
use TypeError;

it('initializes the service with a valid cloud provider', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    expect($service)->toBeInstanceOf(CloudIamServiceAccountKeyServiceFake::class);
});

it('cannot initialize the service with an invalid cloud provider', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'aws')->first();

    if (!$gcp_cloud_provider) {
        throw new Exception('No Cloud Provider with the `aws` type exists in the database.');
    }

    $service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
})->expectExceptionMessage('The GCP service is trying to use a cloud provider that is not the correct type.');

it('can set the project_id class variable to a numeric value', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_id');

    $service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, $project_id);

    $service->setProjectId($project_id);

    expect($service->getProjectId())->toBe($project_id);
});

it('can set the project_id class variable to an alpha-dash value', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_name');

    $service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, $project_id);

    $service->setProjectId($project_id);

    expect($service->getProjectId())->toBe($project_id);
});

it('cannot set the project_id class variable as an array and throws a type error', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->first();

    $service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, [config('tests.gcp.project_name')]);
})->expectException('TypeError');

it('can get a list of service account keys', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service_account_key_service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $service_account_service = new CloudIamServiceAccountService($gcp_cloud_provider->id, config('tests.gcp.project_name'));
    $service_account_key_service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service_account_service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (key list)'
    ]);

    if ($service_account->status->code != 200) {
        throw new \Exception($service_account->object->error->message);
    }

    $service_account_key = $service_account_key_service->store($service_account->object->uniqueId);

    $fetched_key = $service_account_key_service->list($service_account->object->uniqueId);

    expect($fetched_key->status->code)->toBe(200);

    if ($service_account_key->status->code == 200) {
        $service_account_key_service->delete($service_account->object->uniqueId, $service_account_key->object->name, true);
    }

    if ($service_account->status->code == 200) {
        $service_account_service->delete($service_account->object->uniqueId);
    }
});

it('can get a specific service account key', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service_account_key_service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $service_account_service = new CloudIamServiceAccountService($gcp_cloud_provider->id, config('tests.gcp.project_name'));
    $service_account_key_service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service_account_service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (key get)'
    ]);

    if ($service_account->status->code != 200) {
        throw new \Exception($service_account->object->error->message);
    }

    $service_account_key = $service_account_key_service->store($service_account->object->uniqueId);

    $fetched_key = $service_account_key_service->get($service_account->object->uniqueId, $service_account_key->object->name, true);

    expect($fetched_key->status->code)->toBe(200);
    expect($fetched_key->object->keyAlgorithm)->toBe('KEY_ALG_RSA_2048');

    if ($service_account_key->status->code == 200) {
        $service_account_key_service->delete($service_account->object->uniqueId, $service_account_key->object->name, true);
    }

    if ($service_account->status->code == 200) {
        $service_account_service->delete($service_account->object->uniqueId);
    }
});

it('can create a new service account key', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service_account_service = new CloudIamServiceAccountService($gcp_cloud_provider->id, config('tests.gcp.project_name'));
    $service_account_key_service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service_account_service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (key store)'
    ]);

    if ($service_account->status->code != 200) {
        throw new \Exception($service_account->object->error->message);
    }

    $service_account_key = $service_account_key_service->store($service_account->object->uniqueId);

    expect($service_account_key->status->code)->toBe(200);
    expect($service_account_key->object->keyAlgorithm)->toBe('KEY_ALG_RSA_2048');

    if ($service_account_key->status->code == 200) {
        $service_account_key_service->delete($service_account->object->uniqueId, $service_account_key->object->name, true);
    }

    if ($service_account->status->code == 200) {
        $service_account_service->delete($service_account->object->uniqueId);
    }
});

it('can delete a service account key', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service_account_service = new CloudIamServiceAccountService($gcp_cloud_provider->id, config('tests.gcp.project_name'));
    $service_account_key_service = new CloudIamServiceAccountKeyServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service_account_service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (key delete)'
    ]);

    if ($service_account->status->code != 200) {
        throw new \Exception($service_account->object->error->message);
    }

    $service_account_key = $service_account_key_service->store($service_account->object->uniqueId);

    if ($service_account_key->status->code != 200) {
        throw new Exception($service_account_key->object->error->message);
    } else {
        $deleted_service_account_key = $service_account_key_service->delete($service_account->object->uniqueId, $service_account_key->object->name, true);
        expect($deleted_service_account_key->status->code)->toBe(200);
    }

    $service_account_service->delete($service_account->object->uniqueId);
});
