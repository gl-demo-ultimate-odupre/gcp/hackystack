<?php

namespace App\Http\Controllers\Auth\Okta;

use App\Http\Controllers\BaseController;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models;
use App\Services;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Socialite;

class LoginController extends BaseController
{

    use AuthenticatesUsers;

    private $okta_provider;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->okta_provider = Models\Auth\AuthProvider::query()
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->where('slug', 'okta')
            ->firstOrFail();

        Config::set('services.okta', [
            'client_id' => decrypt($this->okta_provider->client_id),
            'client_secret' => decrypt($this->okta_provider->client_secret),
            'redirect' => config('app.url').'/login/okta/callback',
            'base_url' => $this->okta_provider->base_url
        ]);

    }

    public function redirectToProvider(Request $request)
    {
        return Socialite::driver('okta')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {
        // If callback contains an error, we will not proceed any further and
        // return the error description as a flashed alert-danger message.
        // https://developer.okta.com/docs/reference/api/oidc/#possible-errors
        if($request->has('error')) {

            // The ucfirst helper converts `error_code` to human readable `Error code`.
            $alert_prefix = Str::ucfirst(str_replace('_', ' ', $request->error)).'.';

            // If error description is common and is cryptic to users, we can add
            // rephrased error messages here that are displayed in the alert banner.
            if($request->error_description == 'User is not assigned to the client application.') {
                $alert_suffix = 'Your OKTA account does not have permission to access this application.';
            } else {
                $alert_suffix = $request->error_description;
            }

            Log::channel('hackystack-auth')->notice(
                'Failed authentication attempt for unauthorized provider user.',
                [
                    'log_event_type' => 'auth-provider-user-unauthorized',
                    'log_class' => get_class(),
                    'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                    'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                    'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                    'auth_provider_id' => $this->okta_provider->id,
                    'auth_provider_slug' => $this->okta_provider->slug,
                    'provider_error' => $request->error,
                    'provider_error_description' => $request->error_description,
                ]
            );

            // Add the alert message to the session flash to appear in the
            // alert banner on the login page.
            $request->session()->flash('alert-danger', $alert_prefix.' '.$alert_suffix);

            // Redirect to the login page (no authentication takes place).
            return redirect()->route('auth.login');

        // If callback is successful (no errors), then proceed with authenticating
        // the user using a new or existing user account.
        } else {

            // Initialize service for managing user account
            $authUserService = new Services\V1\Auth\AuthUserService();
            $authGroupUserService = new Services\V1\Auth\AuthGroupUserService();
            $cloudAccountUserService = new Services\V1\Cloud\CloudAccountUserService();

            // Get user meta data from OKTA response
            $okta_user = Socialite::driver('okta')->user();

            // Uncomment this for troubleshooting the OKTA response
            //dd($okta_user);

            // Check if user exists based on email address
            $auth_user = Models\Auth\AuthUser::where('email', $okta_user->email)->first();

            // If user does not exist, create a local user with the meta data from OKTA
            if(!$auth_user) {

                // Create record using service method
                $auth_user = $authUserService->store([
                    'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                    'auth_provider_id' => $this->okta_provider->id,
                    'username' => $okta_user->email,
                    'full_name' => $okta_user->name,
                    'email' => $okta_user->email,
                    'provider_token' => $okta_user->token,
                    'provider_meta_data' => $okta_user->user,
                    'password' => Str::random(24),
                    'flag_must_change_password' => 0,
                    'flag_account_verified' => 1,
                ]);

                Log::channel('hackystack-auth')->info(
                    'User record created using provider meta data.',
                    [
                        'log_event_type' => 'auth-provider-user-created',
                        'log_class' => get_class(),
                        'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                        'auth_provider_id' => $this->okta_provider->id,
                        'auth_provider_slug' => $this->okta_provider->slug,
                        'auth_user_id' => $auth_user->id,
                    ]
                );

            }

            // If the user already exists, verify that the token is up-to-date
            // and update the user with meta data provided in request
            elseif($okta_user->token != $auth_user->provider_token) {

                // Update method using service method
                $auth_user = $authUserService->update($auth_user->id, [
                    'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                    'auth_provider_id' => $this->okta_provider->id,
                    'username' => $okta_user->email,
                    'full_name' => $okta_user->name,
                    'email' => $okta_user->email,
                    'provider_token' => $okta_user->token,
                    'provider_meta_data' => $okta_user->user,
                    'flag_must_change_password' => 0,
                    'flag_account_verified' => 1,
                ]);

                Log::channel('hackystack-auth')->info(
                    'User record updated using provider meta data.',
                    [
                        'log_event_type' => 'auth-provider-user-updated',
                        'log_class' => get_class(),
                        'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                        'auth_provider_id' => $this->okta_provider->id,
                        'auth_provider_slug' => $this->okta_provider->slug,
                        'auth_user_id' => $auth_user->id,
                    ]
                );

            }

            //
            // Authentication Provider Fields
            // ----------------------------------------------------------------
            // We lookup all field mappings that have been associated with the
            // provided and see if meta data provided in the OKTA response
            // has a matching key and update the respective auth_user column.
            //

            // Create collection of OKTA meta data
            $okta_meta_data = collect($okta_user->user);

            // Get auth provider fields to map OKTA meta data to user account fields
            $auth_provider_fields = Models\Auth\AuthProviderField::query()
                ->where('auth_tenant_id', $this->okta_provider->auth_tenant_id)
                ->where('auth_provider_id', $this->okta_provider->id)
                ->get();

            // Loop through authentication provider field mappings
            foreach($auth_provider_fields as $provider_field) {

                // Define variables for use in conditional statements below
                $provider_key = $provider_field->provider_key;
                $user_column = $provider_field->user_column;

                // Check if provider key exists in OKTA meta data
                if($okta_meta_data->has($provider_key)) {

                    // If auth_user column is NULL and provider key has a value,
                    // update the auth_user model using the provider meta data
                    if($auth_user->$user_column == null && $okta_meta_data[$provider_key] != null) {
                        $auth_user->$user_column = $okta_meta_data[$provider_key];
                        $auth_user->save();

                        Log::channel('hackystack-auth')->info(
                            'Set user column value using provider field mapping.',
                            [
                                'log_event_type' => 'auth-provider-field-user-updated',
                                'log_class' => get_class(),
                                'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                                'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                                'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                                'auth_provider_id' => $this->okta_provider->id,
                                'auth_provider_slug' => $this->okta_provider->slug,
                                'auth_user_id' => $auth_user->id,
                                'user_column' => $user_column,
                                'provider_key' => $provider_key,
                            ]
                        );

                    }

                    // If auth_user column has a value and provider key is NULL,
                    // assume that auth_user is more accurate than the provider
                    elseif($auth_user->$user_column != null && $okta_meta_data[$provider_key] == null) {
                        // No changes will be made
                    }

                    // If auth_user column is null and provider key is NULL,
                    // assume that no value exists for this column.
                    elseif($auth_user->$user_column == null && $okta_meta_data[$provider_key] == null) {
                        // No changes will be made
                    }

                    // If auth_user column and provider key value both have a
                    // value but don't match, update the auth user model using
                    // the provider meta data.
                    elseif($auth_user->$user_column != $okta_meta_data[$provider_key]) {
                        $auth_user->$user_column = $okta_meta_data[$provider_key];
                        $auth_user->save();

                        Log::channel('hackystack-auth')->info(
                            'Updated user column value using provider field mapping.',
                            [
                                'log_event_type' => 'auth-provider-field-user-updated',
                                'log_class' => get_class(),
                                'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                                'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                                'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                                'auth_provider_id' => $this->okta_provider->id,
                                'auth_provider_slug' => $this->okta_provider->slug,
                                'auth_user_id' => $auth_user->id,
                                'user_column' => $user_column,
                                'provider_key' => $provider_key,
                            ]
                        );
                    }

                    // If auth_user column and provider key value match, this
                    // field is already up-to-date.
                    elseif($auth_user->$user_column == $okta_meta_data[$provider_key]) {
                        // No changes will be made
                    }

                } // if($okta_meta_data->has($provider_key))
            } // foreach($auth_provider_fields

            //
            // Authentication Provider Groups
            // ----------------------------------------------------------------
            // We lookup all groups that have been associated with the provider
            // and see if meta data provided in the OKTA response matches and
            // associate the user with the groups.
            //

            // Get auth provider fields to map OKTA meta data to user account fields
            $auth_provider_groups = Models\Auth\AuthProviderGroup::query()
                ->where('auth_tenant_id', $this->okta_provider->auth_tenant_id)
                ->where('auth_provider_id', $this->okta_provider->id)
                ->get();

            // Loop through authentication provider groups
            foreach($auth_provider_groups as $provider_group) {

                if($provider_group->type == 'default') {

                    // Check if user has already been associated with group
                    $existing_group = Models\Auth\AuthGroupUser::query()
                        ->where('auth_group_id', $provider_group->auth_group_id)
                        ->where('auth_user_id', $auth_user->id)
                        ->first();

                    // If user has not been associated with group
                    if(!$existing_group) {

                        // Create new group relationship for user
                        $authGroupUserService->store([
                            'auth_tenant_id' => $auth_user->auth_tenant_id,
                            'auth_group_id' => $provider_group->auth_group_id,
                            'auth_user_id' => $auth_user->id,
                        ]);

                        Log::channel('hackystack-auth')->info(
                            'Granted access to group using provider default group mapping.',
                            [
                                'log_event_type' => 'auth-provider-group-default-user-created',
                                'log_class' => get_class(),
                                'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                                'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                                'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                                'auth_provider_id' => $this->okta_provider->id,
                                'auth_provider_slug' => $this->okta_provider->slug,
                                'auth_user_id' => $auth_user->id,
                                'auth_group_id' => $provider_group->auth_group_id,
                                'auth_group_slug' => $provider_group->authGroup->slug,
                            ]
                        );

                    } // if(!$existing_group)

                } elseif($provider_group->type == 'meta') {

                    // Define variables for use in conditional statements below
                    $meta_key = $provider_group->meta_key;
                    $meta_value = $provider_group->meta_value;

                    // Check if provider key exists in OKTA meta data
                    if($okta_meta_data->has($meta_key)) {

                        // Check if the values match
                        if($okta_meta_data[$meta_key] == $meta_value) {

                            // Check if user has already been associated with group
                            $existing_group = Models\Auth\AuthGroupUser::query()
                                ->where('auth_group_id', $provider_group->auth_group_id)
                                ->where('auth_user_id', $auth_user->id)
                                ->first();

                            // If user has not been associated with group
                            if(!$existing_group) {

                                // Create new group relationship for user
                                $authGroupUserService->store([
                                    'auth_tenant_id' => $auth_user->auth_tenant_id,
                                    'auth_group_id' => $provider_group->auth_group_id,
                                    'auth_user_id' => $auth_user->id,
                                ]);

                                Log::channel('hackystack-auth')->info(
                                    'Granted access to group using provider meta data mapping.',
                                    [
                                        'log_event_type' => 'auth-provider-group-meta-user-created',
                                        'log_class' => get_class(),
                                        'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                                        'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                                        'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                                        'auth_provider_id' => $this->okta_provider->id,
                                        'auth_provider_slug' => $this->okta_provider->slug,
                                        'auth_user_id' => $auth_user->id,
                                        'auth_group_id' => $provider_group->auth_group_id,
                                        'auth_group_slug' => $provider_group->authGroup->slug,
                                    ]
                                );

                            } // if(!$existing_group)

                            // TODO Evaluate how expires_at is considered for
                            // rejoining a group with a new authentication attempt.

                        } // if($okta_meta_data[$meta_key] == $meta_value)
                    } // if($okta_meta_data->has($meta_key))
                } // elseif($provider_group->type == 'meta')
            } // foreach($auth_provider_groups)

            // Provision GitOps user account
            if($auth_user->flag_git_user_provisioned == 0) {
                if($auth_user->authTenant->git_provider_base_url != null && $auth_user->authTenant->git_provider_api_token != null) {

                    $authUserService->provisionGitUser($auth_user->id);
                }
            }

            // Loop through Cloud Accounts
            foreach($auth_user->cloudAccountUsers as $cloud_account_user) {

                // Provision GitOps user account
                if($cloud_account_user->flag_git_group_user_provisioned == 0) {
                    if($cloud_account_user->authUser->authTenant->git_provider_base_url != null && $cloud_account_user->authUser->authTenant->git_provider_api_token != null) {
                        $cloudAccountUserService->provisionGitGroupUser($cloud_account_user->id);
                    }
                }

            }

            // Refresh user record
            $auth_user = $auth_user->fresh();

        }

        try {

            // Use Laravel handler to create authenticated session
            Auth::login($auth_user);

            Log::channel('hackystack-auth')->info(
                'User authentication successful.',
                [
                    'log_event_type' => 'auth-login-success',
                    'log_class' => get_class(),
                    'ip_address' => config('hackystack.log.ip_address.enabled') ? $request->ip() : null,
                    'user_agent' => config('hackystack.log.user_agent.enabled') ? $request->userAgent() : null,
                    'auth_tenant_id' => $this->okta_provider->auth_tenant_id,
                    'auth_provider_id' => $this->okta_provider->id,
                    'auth_provider_slug' => $this->okta_provider->slug,
                    'auth_user_id' => $auth_user->id,
                ]
            );

        } catch (\Throwable $e) {

            report($e);

            // Show error authentication fails (shouldn't with earlier handling)
            abort(500, 'There was a system error when trying to authenticate. We have received the bug report and will investigate.');

            return redirect()->route('auth.login');
        }

        return redirect('/dashboard');
    }

}
