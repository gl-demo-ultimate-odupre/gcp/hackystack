<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DashboardController extends BaseController
{
    public function __construct()
    {
        //
    }

    public function show(Request $request)
    {

        // TODO Additional validation of admin roles and permissions

        return view('admin.dashboard.show', compact([
            'request'
        ]));

    }

}
