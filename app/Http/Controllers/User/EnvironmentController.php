<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class EnvironmentController extends BaseController
{
    public function __construct()
    {
        // TODO Additional validation of roles and permissions
    }

    public function index(Request $request)
    {

        $auth_user_id = $request->user()->id;

        $cloud_account_users = Models\Cloud\CloudAccountUser::query()
            ->with([
                'authTenant',
                'cloudAccount',
                'cloudAccount.cloudAccountEnvironments'
            ])
            ->where('auth_user_id', $request->user()->id)
            ->get();

        return view('user.environments.index', compact([
            'request',
            'cloud_account_users',
        ]));
    }

    public function create(Request $request)
    {

        $auth_user_id = $request->user()->id;

        $cloud_accounts = Models\Cloud\CloudAccount::query()
            ->whereHas('cloudProvider', function ($query) {
                $query->where('type', 'gcp');
            })->whereHas('cloudAccountUsers', function ($query) use ($auth_user_id) {
                $query->where('auth_user_id', $auth_user_id);
            })->orderBy('cloud_provider_id')
            ->orderBy('slug')
            ->get();

        // Set a null value for conditional queries
        $cloud_account = null;
        $environment_templates = null;
        $gcp_service_usage_api_enabled = false;
        $gcp_apis_enabled = [
            'compute' => false,
            'dns' => false,
            'iam' => false,
            // Additional services can be added here ({name}.googleapis.com)
        ];

        // If cloud provider has been specified on the page
        if ($request->has('cloud_account_id')) {

            // Lookup Cloud Account
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->with([
                    'cloudProvider'
                ])->whereHas('cloudAccountUsers', function ($query) use ($auth_user_id) {
                    $query->where('auth_user_id', $auth_user_id);
                })->where('id', $request->cloud_account_id)
                ->firstOrFail();

            // If Cloud Account is a GCP Project
            if ($cloud_account->cloudProvider->type == 'gcp') {

                // Instantiate Service Usage API to check if services are enabled
                $service_usage_api_service = new \App\Services\V1\Vendor\Gcp\ServiceUsageApiService(
                    $cloud_account->cloud_provider_id,
                    $cloud_account->api_meta_data['name']
                );

                // Check if service usage API is enabled
                $gcp_service_usage_api_enabled = $service_usage_api_service->checkIfServiceUsageApiEnabled();

                // Check if various service APIs are enabled that are used for
                // Terraform Environments with GitOps and CI/CD.
                if ($gcp_service_usage_api_enabled == true) {
                    foreach ($gcp_apis_enabled as $service => $state) {
                        $gcp_apis_enabled[$service] = $service_usage_api_service->checkIfServiceEnabled($service . '.googleapis.com');
                    }
                }
            }

            $environment_templates = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('state', 'active')
                ->where('cloud_provider_id', $cloud_account->cloud_provider_id)
                ->get();
        }

        return view('user.environments.create', compact([
            'request',
            'cloud_accounts',
            'cloud_account',
            'environment_templates',
            'gcp_service_usage_api_enabled',
            'gcp_apis_enabled',
        ]));
    }

    /**
     *   Store a Cloud Account Environment
     *
     *   @param  Request $request
     *      cloud_provider_id|uuid
     *      cloud_organization_unit_id|uuid
     *      account_name_prefix|string
     *      account_name_suffix|string
     */
    public function store(Request $request)
    {

        // Validate form request input matches rules in model
        $validator = Validator::make(
            $request->all(),
            [
                // Validation rules
                'cloud_account_id' => 'required|uuid|exists:cloud_accounts,id',
                'cloud_account_environment_template_id' => 'required|uuid|exists:cloud_accounts_environments_templates,id',
                'name' => 'required|alpha_dash|max:30',
            ],
            [
                // Custom error messages
                // 'short_id.required' => 'You have not provided an invitation token.',
            ]
        )->validate();

        // Lookup cloud account to get relationship IDs
        $cloud_account = Models\Cloud\CloudAccount::query()
            ->where('id', $request->cloud_account_id)
            ->firstOrFail();

        // Lookup cloud account environment template to get relationship IDs
        $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
            ->where('id', $request->cloud_account_environment_template_id)
            ->firstOrFail();

        // Define variable for environment name
        $name = $request->name;

        // Use service to create new Cloud Account Environment
        $cloudAccountEnvironmentService = new Services\V1\Cloud\CloudAccountEnvironmentService();
        $cloud_account_environment = $cloudAccountEnvironmentService->store([
            'auth_tenant_id' => $cloud_account->auth_tenant_id,
            'cloud_account_id' => $cloud_account->id,
            'cloud_account_environment_template_id' => $cloud_account_environment_template->id,
            'cloud_organization_unit_id' => $cloud_account->cloud_organization_unit_id,
            'cloud_provider_id' => $cloud_account->cloud_provider_id,
            'cloud_realm_id' => $cloud_account->cloud_realm_id,
            'name' => $name,
        ]);

        return redirect()->route('user.cloud.accounts.show', ['id' => $cloud_account_environment->cloud_account_id]);
    }
}
