<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudBillingOrganizationService
{

    use BaseService;

    public function __construct($cloud_provider_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);
        $this->setGoogleApiClient();

        // Add API Scope(s)
        // https://developers.google.com/identity/protocols/oauth2/scopes
        // https://github.com/googleapis/google-api-php-client-services/blob/master/src/Google/Service/Cloudbilling.php
        $this->google_api_client->addScope(\Google_Service_Cloudbilling::CLOUD_BILLING);
    }

    /**
     *   Get list of billing accounts for organization
     *
     *   @see https://cloud.google.com/billing/docs/reference/rest/v1/billingAccounts/list
     *   @see https://github.com/googleapis/google-api-php-client-services/blob/master/src/Cloudbilling/Resource/BillingAccounts.php#L125
     *
     *   @param  array  $request_data
     *      see vendor docs for available options
     *
     *   @return array                  API Meta Data (Response)
     *      [
     *          displayName: "My First Billing Account",
     *          masterBillingAccount: "",
     *          name: "billingAccounts/A1B2C3-D4E5F6-A7B8C9",
     *          open: true,
     *      ],
     *      [
     *          displayName: "My Second Billing Account",
     *          masterBillingAccount: "",
     *          name: "billingAccounts/F9E8D7-C6B5A4-F3E2D1",
     *          open: false,
     *      ],
     */
    public function list($request_data = [])
    {
        // Initialize the Billing API Service
        $google_cloud_billing_service = new \Google_Service_Cloudbilling($this->google_api_client);

        try {

            // Get list of billing accounts from API
            $billing_account_api_response = $google_cloud_billing_service->billingAccounts->listBillingAccounts($request_data);

        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        return (array) $billing_account_api_response['billingAccounts'];

    }

    /**
     *   Get an existing billing account
     *
     *   @see https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/CloudResourceManager
     *   @see https://cloud.google.com/billing/docs/reference/rest/v1/billingAccounts/get
     *
     *   @param  array  $request_data
     *      billing_account_name|string          Ex. billingAccounts/A1B2C3-D4E5F6-A7B8C9 or A1B2C3-D4E5F6-A7B8C9
     *
     *   @return array                  API Meta Data (Response)
     *      displayName: "My First Billing Account",
     *      masterBillingAccount: "",
     *      name: "billingAccounts/A1B2C3-D4E5F6-A7B8C9",
     *      open: true,
     */
    public function get($request_data = [])
    {

        // Initialize the Billing API Service
        $google_cloud_billing_service = new \Google_Service_Cloudbilling($this->google_api_client);

        // Check if billing_account_name has required prefix of `billingAccounts/`
        if(Str::startsWith($request_data['billing_account_name'], 'billingAccounts/')) {
            $billing_account_name = $request_data['billing_account_name'];
        } else {
            $billing_account_name = 'billingAccounts/'.$request_data['billing_account_name'];
        }

        try {

            // Get billing account details from API
            $billing_account_api_response = $google_cloud_billing_service->billingAccounts->get($billing_account_name);

        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        return (array) $billing_account_api_response;

    }

}
