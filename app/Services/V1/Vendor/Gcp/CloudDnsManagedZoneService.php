<?php

namespace App\Services\V1\Vendor\Gcp;

use Exception;
use Glamstack\GoogleCloud\ApiClient;

class CloudDnsManagedZoneService
{
    use BaseService;

    const API_SCOPES = [
        'https://www.googleapis.com/auth/ndev.clouddns.readwrite'
    ];

    private ApiClient $api_client;

    public function __construct($cloud_provider, string $project_id)
    {
        $this->setCloudProvider($cloud_provider);

        $this->setJsonKeyFilePath();

        $this->setProjectId($project_id);

        $this->api_client = $this->setGlamstackGoogleCloudSdk(
            self::API_SCOPES,
            $this->json_key_file_path,
            $this->project_id
        );
    }

    /**
     * Create GCP DNS Managed Zone
     *
     * $request_data Parameters:
     * ```php
     * [
     *     'managed_zone' => Required | (string) GCP Managed Zone name
     *     'fqdn' => Required | (string) The FQDN of the managed zone ("example-zone.example.com.")
     *     'visibility' => Required | (string) The zone's visibility: public zones are exposed to the Internet while private zones are visible only to Virtual Private Cloud resources. ("public" or "private"),
     *     'logging_enabled' => Required | (bool) Enables or Disabled logging
     *     'description' => Required | (string) A short description of the managed zone
     * ];
     * ```
     *
     * @see https://cloud.google.com/dns/docs/zones
     *
     * @see https://cloud.google.com/dns/docs/reference/v1/managedZones/create
     *
     * @param array $request_data
     *      Array of options for creating a GCP DNS Managed Zone
     *
     * @return object
     *
     * @throws Exception
     */
    public function createDnsManagedZone(array $request_data): object
    {
        $request_data['project_id'] = $request_data['project_id'] ?? $this->project_id;

        // Check if zone exists
        $zone_exists = $this->checkForDnsZone($request_data);

        // Create zone if not
        if ($zone_exists->status->code == 404) {
            $response = $this->createGcpDnsManagedZone($request_data);
            return $response;
        } else {
            if (property_exists($zone_exists->object, 'error')) {
                throw new Exception($zone_exists->object->error->message);
            } else {
                throw new Exception($zone_exists->json);
            }
        }
    }

    /**
     * Check if GCP Managed Zone exists
     *
     * @param array $request_data
     *      The request data to send into the Managed Zone API GET request
     *
     * @return object|string
     */
    protected function checkForDnsZone(array $request_data): object|string
    {
        $request_data['project_id'] = $request_data['project_id'] ?? $this->project_id;

        return $this->api_client->dns()->managedZone()->get([
            'managed_zone' => $request_data['name'],
            'project_id' => $request_data['project_id']
        ]);
    }

    /**
     * Utilize SDK to create a Managed Zone
     *
     * @param $request_data
     *      Validated Request Data to send to the Managed Zone POST API
     *
     * @return object|string
     *      Will return object of successful else will return failure message
     *
     * @throws Exception
     */
    protected function createGcpDnsManagedZone($request_data): object|string
    {
        return $this->api_client->dns()->managedZone()->create([
            'name' => $request_data['name'],
            'dns_name' => $request_data['fqdn'],
            'visibility' => $request_data['visibility'],
            'logging_enabled' => $request_data['logging_enabled'],
            'description' => $request_data['description']
        ]);
    }

    /**
     * Utilize SDK to delete a Managed Zone
     *
     * @param $request_data
     *      Validated Request Data to send to the Managed Zone DELETE API
     *
     * @return object|string|void
     */
    public function deleteDnsManagedZone($request_data)
    {
        $request_data['project_id'] = $request_data['project_id'] ?? $this->project_id;

        // Check if zone exists
        $zone_exists = $this->checkForDnsZone($request_data);

        if ($zone_exists->status->code == '200') {
            $response = $this->api_client->dns()->managedZone()->delete([
                'managed_zone' => $request_data['name'],
            ]);
            return $response;
        } else {
            if (property_exists($zone_exists->object, 'error')) {
                throw new Exception($zone_exists->object->error->message);
            } else {
                throw new Exception($zone_exists->json);
            }
        }
    }
}
