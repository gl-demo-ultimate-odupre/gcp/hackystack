<?php

namespace App\Services\V1\Vendor\Gitlab;

use App\Models;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class BaseService
{

    protected $auth_tenant;
    protected $gitlab_api_client;

    /**
     *   Set the Auth Tenant used for GitLab API calls
     *
     *   @param uuid $auth_tenant_id
     */
    public function setAuthTenant($auth_tenant_id)
    {
        // Get auth tenant from database
        $this->auth_tenant = Models\Auth\AuthTenant::query()
            ->where('id', $auth_tenant_id)
            ->firstOrFail();

        // Check if Git provider is configured
        if($this->checkIfGitProviderIsConfigured() == false) {
            abort(500, 'The GitLab provider has not been configured for the tenant.', [
                'auth_tenant_id' => $this->auth_tenant->id
            ]);
        }
    }

    /**
     *   Initialize GitLab API client
     */
    public function setGitlabApiClient()
    {
        // Initialize GitLab API connection with HTTP (Personal Access Token)
        // https://github.com/GitLabPHP/Client
        $this->gitlab_api_client = new \Gitlab\Client();
        $this->gitlab_api_client->authenticate(decrypt($this->auth_tenant->git_provider_api_token), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $this->gitlab_api_client->setUrl($this->auth_tenant->git_provider_base_url);
    }

    /**
     *   Handle GitLab API Exception
     *
     *   @param  [type] $e              An instance of the exception
     *   @param  string $log_class      get_class()
     *   @param  string $reference      Reference slug or identifier
     *
     *   @return string                 Error message
     */
    public function handleException($e, $log_class, $reference)
    {
        //$error_message = json_decode($e);

        dd($e);

        Log::channel('hackystack-git')->error('GitLab API returned a response error.', [
            'log_event_type' => 'gitlab-api-response-error',
            'log_class' => get_class(),
            'auth_tenant_id' => $this->auth_tenant->id,
            'error_code' => $error_message->code,
            'error_message' => $error_message->message,
            'error_reference' => $reference,
        ]);

        return $error_message;

    }

    /**
     *   Check if Git Provider is Configured
     *
     *   This method checks each of the AuthTenant fields for the configuration
     *   values of the Git provider and ensures that the meta data array with
     *   the top-level group has been populated.
     *
     *   @return bool
     */
    public function checkIfGitProviderIsConfigured()
    {
        // Check if cloud provider type has been specified
        if($this->auth_tenant->git_provider_type == null) {
            return false;
        }

        // Check if cloud provider type is not correct
        elseif($this->auth_tenant->git_provider_type != 'gitlab') {

            Log::channel('hackystack-git')->error('The AuthTenant git_provider_type is not the correct type for the GitLab service.', [
                'log_event_type' => 'invalid-git-provider-type',
                'log_class' => get_class(),
                'auth_tenant_id' => $this->auth_tenant->id,
                'git_provider_type' => $this->auth_tenant->git_provider_type
            ]);

            return false;
        }

        // Check if URL is set
        elseif($this->auth_tenant->git_provider_base_url == null) {

            Log::channel('hackystack-git')->error('The AuthTenant git_provider_base_url has not be set in the database so we cannot connect to the API.', [
                'log_event_type' => 'git-provider-base-url-not-set',
                'log_class' => get_class(),
                'auth_tenant_id' => $this->auth_tenant->id,
            ]);

            return false;
        }

        // Check if API token is set
        elseif($this->auth_tenant->git_provider_api_token == null) {

            Log::channel('hackystack-git')->error('The AuthTenant git_provider_api_token has not be set in the database so we cannot connect to the API.', [
                'log_event_type' => 'git-provider-api-token-not-set',
                'log_class' => get_class(),
                'auth_tenant_id' => $this->auth_tenant->id,
            ]);

            return false;
        }

        // Check if group ID is set
        elseif($this->auth_tenant->git_provider_base_group_id == null) {

            Log::channel('hackystack-git')->error('The AuthTenant git_provider_base_group_id has not be set in the database.', [
                'log_event_type' => 'git-provider-base-group-id-not-set',
                'log_class' => get_class(),
                'auth_tenant_id' => $this->auth_tenant->id,
            ]);

            return false;
        }

        // Check if meta data is populated
        elseif($this->auth_tenant->git_meta_data == null) {

            // Initialize API client
            $this->setGitlabApiClient();

            // Use the API to get the details of the existing GitLab group
            // https://docs.gitlab.com/ee/api/groups.html#details-of-a-group
            $gitlab_group = $this->gitlab_api_client->groups()->show($this->auth_tenant->git_provider_base_group_id);

            //
            // Remove select keys from the API response
            // ------------------------------------------------
            // We remove nested array keys from API result since
            // they contain unnecessary meta data and throw
            // errors with nested array outputs when parsing
            // as a string.
            //

            if(array_key_exists('shared_with_groups', $gitlab_group)) {
                unset($gitlab_group['shared_with_groups']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('projects', $gitlab_group)) {
                unset($gitlab_group['projects']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('shared_projects', $gitlab_group)) {
                unset($gitlab_group['shared_projects']);
            }

            // Check if ID is in group response
            if(!array_key_exists('id', $gitlab_group)) {

                Log::channel('hackystack-git')->error('The AuthTenant git_meta_data was not set and had invalid API response.', [
                    'log_event_type' => 'auth-tenant-git-meta-data-invalid',
                    'log_class' => get_class(),
                    'auth_tenant_id' => $this->auth_tenant->id,
                    'api_response' => $gitlab_group
                ]);

                return false;
            }

            // If ID is not present, assume an error message or unknown result
            else {

                // Update AuthTenant with group meta data
                $this->auth_tenant->git_meta_data = $gitlab_group;
                $this->auth_tenant->save();

                Log::channel('hackystack-git')->info('The AuthTenant git_meta_data was not set and has been updated using API response.', [
                    'log_event_type' => 'auth-tenant-git-meta-data-updated',
                    'log_class' => get_class(),
                    'auth_tenant_id' => $this->auth_tenant->id,
                ]);

                return true;
            }

        }

        // If all conditionals pass, then assume true
        else {
            return true;
        }

    }

}
