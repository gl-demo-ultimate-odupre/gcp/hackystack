<?php

namespace App\Services\V1\Vendor\Aws;

use App\Services\V1\Vendor\Aws\BaseService;
use Aws\Organizations\OrganizationsClient;
use Aws\Organizations\Exception\OrganizationsException;
use Illuminate\Support\Facades\Log;

class OrganizationAccountService extends BaseService
{
    protected $aws_api_client;

    public function __construct($cloud_provider_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);

        // Decrypt the provider credentials to be able to use the array keys
        $credentials = json_decode(decrypt($this->cloud_provider->api_credentials), true);

        // Initialize the API client
        $this->aws_api_client = new OrganizationsClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'credentials' => [
                'key'    => $credentials['aws_access_key_id'],
                'secret' => $credentials['aws_access_key_secret'],
            ],
        ]);
    }

    /**
     * Get list of all accounts
     *
     * @param  array  $request_data
     *    parent_id|string (opt)  Root (r-####) or Org Unit ID (ou-1a2b-34uvwxyz)
     *
     * @return array
     *    'Arn' => '<string>',
     *    'Email' => '<string>',
     *    'Id' => '<string>',
     *    'JoinedMethod' => 'INVITED|CREATED',
     *    'JoinedTimestamp' => <DateTime>,
     *    'Name' => '<string>',
     *    'Status' => 'ACTIVE|SUSPENDED',
     */
    public function all($request_data = [])
    {
        if (array_key_exists('parent_id', $request_data)) {
            $records = $this->aws_api_client->listAccountsForParent([
                'ParentId' => $request_data['parent_id']
            ]);
        } else {
            $records = $this->aws_api_client->listAccounts([]);
        }

        return $records;
    }

    /**
     * Create a AWS account
     *
     * @see https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-organizations-2016-11-28.html#createaccount
     *
     * @param array $request_data
     *    name|string                 Friendly name for AWS account
     *    email|string                Email for root account (AWS globally unique)
     *    billing_access|bool (opt)   If true, non-root users can access billing
     *    role|string (opt)           Name of custom role for parent account to assume admin privileges
     *    tags|array (opt)
     *        [
     *            'Key' => '<string>',
     *            'Value' => '<string>'
     *        ]
     *
     * @return array CreateAccountStatus array
     *    'AccountId' => '<string>',
     *    'AccountName' => '<string>',
     *    'CompletedTimestamp' => <DateTime>,
     *    'FailureReason' =>  'ACCOUNT_LIMIT_EXCEEDED|EMAIL_ALREADY_EXISTS|INVALID_ADDRESS|INVALID_EMAIL|CONCURRENT_ACCOUNT_MODIFICATION|INTERNAL_FAILURE|GOVCLOUD_ACCOUNT_ALREADY_EXISTS|MISSING_BUSINESS_VALIDATION|MISSING_PAYMENT_INSTRUMENT',
     *    'GovCloudAccountId' => '<string>',
     *    'Id' => '<string>',
     *    'RequestedTimestamp' => <DateTime>,
     *    'State' => 'IN_PROGRESS|SUCCEEDED|FAILED',
     */
    public function create($request_data = [])
    {
        $create_account_status = $this->aws_api_client->createAccount([
            'AccountName' => $request_data['name'],
            'Email' => $request_data['email'],
            'IamUserAccessToBilling' => array_key_exists('billing_access', $request_data) && $request_data['billing_access'] == true ? 'ALLOW' : 'DENY',
            'RoleName' => array_key_exists('role', $request_data) ? $request_data['role'] : 'OrganizationAccountAccessRole',
            'Tags' => array_key_exists('tags', $request_data) ? $request_data['tags'] : [],
        ]);

        return $create_account_status['CreateAccountStatus'];
    }

    /**
     * Get the status of AWS account creation request
     *
     * @see https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-organizations-2016-11-28.html#shape-createaccountstatus
     *
     * @param  string $id
     *    Create Account Request operation ID
     *
     * @return array Array return if State is FAILED
     *    'State' => 'FAILED|IN_PROGRESS',
     *    'AccountId' => '<string>',
     *    'AccountName' => '<string>',
     *    'CompletedTimestamp' => <DateTime>,
     *    'Email' => '<string>',
     *    'FailureReason' => 'ACCOUNT_LIMIT_EXCEEDED|EMAIL_ALREADY_EXISTS|INVALID_ADDRESS|INVALID_EMAIL|CONCURRENT_ACCOUNT_MODIFICATION|INTERNAL_FAILURE|GOVCLOUD_ACCOUNT_ALREADY_EXISTS|MISSING_BUSINESS_VALIDATION|MISSING_PAYMENT_INSTRUMENT',
     *    'Id' => '<string>',
     *    'JoinedMethod' => 'INVITED|CREATED',
     *    'JoinedTimestamp' => <DateTime>,
     *    'RequestedTimestamp' => <DateTime>,
     *
     * @return array Array returned if State is SUCCEEDED
     *    'State' => 'SUCCEEDED',
     *    'Id' => '<string>',
     *    'Name' => '<string>',
     *    'Arn' => '<string>',
     *    'Email' => '<string>',
     *    'JoinedMethod' => 'INVITED|CREATED',
     *    'JoinedTimestamp' => <DateTime>,
     *    'Status' => 'ACTIVE|SUSPENDED',
     */
    public function getCreateStatus($id)
    {
        $account_status = $this->aws_api_client->describeCreateAccountStatus([
            'CreateAccountRequestId' => $id
        ]);

        // If operation has completed, return the Account array
        if ($account_status['CreateAccountStatus']['State'] == 'SUCCEEDED') {

            // Get account details
            $account = $this->aws_api_client->describeAccount([
                'AccountId' => $account_status['CreateAccountStatus']['AccountId']
            ]);

            $result = $account['Account'];

            // Get tags for AWS account
            $tags = $this->aws_api_client->listTagsForResource([
                'ResourceId' => $account_status['CreateAccountStatus']['AccountId']
            ]);

            // Add tags to returned array
            $result['Tags'] = $tags;

            // Add CreateAccountStatus state to array for error handling
            // with the state key.
            $result['State'] = 'SUCCEEDED';

            return $result;
        }

        // If operation failed or is in progress
        else {
            return $account_status['CreateAccountStatus'];
        }
    }

    /**
     * Get an AWS Account
     *
     * @see https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-organizations-2016-11-28.html#describeaccount
     *
     * @param string $id
     *    Account ID
     *
     * @return array
     *    'AccountId' => '<string>',
     *    'AccountName' => '<string>',
     *    'Arn' => '<string>',
     *    'Email' => '<string>',
     *    'Id' => '<string>',
     *    'JoinedMethod' => 'INVITED|CREATED',
     *    'JoinedTimestamp' => <DateTime>,
     *    'Status' => 'ACTIVE|SUSPENDED',
     *    'Tags' => [
     *        [
     *            'Key' => '<string>',
     *            'Value' => '<string>'
     *        ]
     *    ]
     */
    public function get($id)
    {
        $record = $this->aws_api_client->describeAccount([
            'AccountId' => $id,
        ]);

        // Get tags for AWS account
        $tags = $this->aws_api_client->listTagsForResource([
            'ResourceId' => $id
        ]);

        // Add tags to returned array
        $record['Account']['Tags'] = $tags;

        return $record['Account'];
    }

    public function update($id, $request_data = [])
    {
        // moveAccount(array $args = [])
        // tagResource(array $args = [])
        // untagResource(array $args = [])
    }

    public function delete($id)
    {
        // leaveOrganization(array $args = [])
        // removeAccountFromOrganization(array $args = [])
    }

    /**
     * Move an AWS account to a different organization unit
     *
     * @see
     *
     * @param string $id
     *      AWS Account ID (12 digits)
     *
     * @param string $organization_unit_id
     *      AWS Organization Unit ID
     *      Example. `ou-a1b2-c3d4e5f6g7`
     */
    public function move(string $id, string $organization_unit_id)
    {
        try {
            $this->aws_api_client->describeAccount([
                'AccountId' => $id
            ]);

            $existing_organization_unit_id = $this->aws_api_client->listParents(['ChildId' => $id])['Parents'][0]['Id'];
        } catch (OrganizationsException $e) {
            Log::error("The AWS account could not be moved due to a validation error.", [
                'aws_account_id' => $id,
                'aws_error' => $e->getAwsErrorMessage()
            ]);
            return false;
        }

        // Get new organization unit
        try {
            $organization_unit = $this->aws_api_client->describeOrganizationalUnit([
                'OrganizationalUnitId' => $organization_unit_id
            ]);
        } catch (OrganizationsException $e) {
            Log::error("The AWS organization unit could not be found when trying to move an AWS account due to a validation error.", [
                'aws_account_id' => $id,
                'aws_organization_unit_id' => $organization_unit_id,
                'aws_error' => $e->getAwsErrorMessage()
            ]);
            return false;
        }

        // Validation that account isn't already in organization unit
        if ($existing_organization_unit_id == $organization_unit_id) {
            Log::notice("The AWS account already belongs to this organization unit and does not need to be moved.", [
                'aws_account_id' => $id,
                'organization_unit_id' => $organization_unit_id,
                'organization_unit_name' => $organization_unit['OrganizationalUnit']['Name']
            ]);
            return true;
        }

        // Move AWS account
        try {
            $this->aws_api_client->moveAccount([
                'AccountId' => $id,
                'DestinationParentId' => $organization_unit_id,
                'SourceParentId' => $existing_organization_unit_id
            ]);
            Log::info("The AWS account has successfully been moved to a new organization unit.", [
                'aws_account_id' => $id,
                'aws_organization_unit_id_old' => $existing_organization_unit_id,
                'aws_organization_unit_id_new' => $organization_unit_id
            ]);
        } catch (OrganizationsException $e) {
            Log::error("The AWS account could not be moved to a different organization unit due to a validation error.", [
                'aws_account_id' => $id,
                'aws_organization_unit_id' => $organization_unit_id,
                'aws_error' => $e->getAwsErrorMessage()
            ]);
            return false;
        }

        return true;
    }
}
