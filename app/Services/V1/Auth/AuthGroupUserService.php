<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthGroupUserService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthGroupUser::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          nullable|uuid|exists:auth_tenants,id
     *      auth_group_id           required|uuid|exists:auth_groups,id
     *      auth_user_id            required|uuid|exists:auth_users,id
     *      expires_at              nullable|datetime
     *      flag_is_manager         nullable|integer|between:0,1
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Get Group relationship
        if(!empty($request_data['auth_group_id'])) {

            // Get relationship by ID to validate that it exists
            $group = Models\Auth\AuthGroup::query()
                ->where('id', $request_data['auth_group_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_group_id = $group->id;
        }

        // Get User relationship
        if(!empty($request_data['auth_user_id'])) {

            // Get relationship by ID to validate that it exists
            $user = Models\Auth\AuthUser::query()
                ->where('id', $request_data['auth_user_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_user_id = $user->id;
        }

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

            if($request_data['expires_at'] == null) {
                $record->expires_at = null;
            } elseif($expires_at > now()) {
                $record->expires_at = $expires_at;
            } elseif($expires_at <= now()) {
                abort(400, 'The `expires_at` value for the group role cannot be in the past.');
            }

        }

        // Determine if flag has been set
        if(!empty($request_data['flag_is_manager'])) {

            if($request_data['flag_is_manager'] == 1) {
                $record->flag_is_manager = 1;
                // TODO Add audit log for elevated permission change
            } else {
                $record->flag_is_manager = 0;
            }

        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      expires_at             nullable|datetime
     *      flag_is_manager        nullable|integer|between:0,1
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            // If request value is different than record existing value
            if($record->expires_at != $request_data['expires_at']) {

                $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

                if($request_data['expires_at'] == null) {
                    $record->expires_at = null;
                } elseif($expires_at > now()) {
                    $record->expires_at = $expires_at;
                } elseif($expires_at <= now()) {
                    abort(400, 'The `expires_at` value for the group role cannot be in the past.');
                }

            }
        }

        // Determine if flag has been set
        if(!empty($request_data['flag_is_manager'])) {

            // If request value is different than record existing value
            if($record->flag_is_manager != $request_data['flag_is_manager']) {

                if($request_data['flag_is_manager'] == 1) {
                    $record->flag_is_manager = 1;
                    // TODO Add audit log for elevated permission change
                } else {
                    $record->flag_is_manager = 0;
                }

            }
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
