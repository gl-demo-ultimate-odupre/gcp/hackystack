<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudAccountGroupRoleService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudAccountGroupRole::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_group_id           required|uuid|exists:auth_groups,id
     *      auth_role_id            required|uuid|exists:auth_roles,id
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      cloud_account_id        required|uuid|exists:cloud_accounts,id
     *      cloud_realm_id          required|uuid|exists:cloud_realms,id
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get Auth Group relationship
        if(!empty($request_data['auth_group_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('id', $request_data['auth_group_id'])
                ->where('auth_tenant_id', $auth_tenant->id)
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_group_id = $auth_group->id;
        }

        // Get Auth Role relationship
        if(!empty($request_data['auth_role_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_role = Models\Auth\AuthRole::query()
                ->where('id', $request_data['auth_role_id'])
                ->where('auth_tenant_id', $auth_tenant->id)
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_role_id = $auth_role->id;
        }

        // Get Cloud Realm relationship
        if(!empty($request_data['cloud_realm_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('id', $request_data['cloud_realm_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_realm_id = $cloud_realm->id;
        }

        // Get Cloud Account relationship
        if(!empty($request_data['cloud_account_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('id', $request_data['cloud_account_id'])
                ->where('cloud_realm_id', $cloud_realm->id)
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_account_id = $cloud_account->id;
        }

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

            if($request_data['expires_at'] == null) {
                $record->expires_at = null;
            } elseif($expires_at > now()) {
                $record->expires_at = $expires_at;
            } elseif($expires_at <= now()) {
                abort(400, 'The `expires_at` value for the group role cannot be in the past.');
            }

        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      auth_group_id               nullable|uuid|exists:auth_groups,id
     *      cloud_account_id            nullable|uuid|exists:cloud_accounts,id
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Get Auth Group relationship
        if(!empty($request_data['auth_group_id'])) {

            // If request data value is different than record existing value
            if($record->auth_group_id != $request_data['auth_group_id']) {

                // Get relationship by ID to validate that it exists
                $auth_group = Models\Auth\AuthGroup::query()
                    ->where('id', $request_data['auth_group_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->auth_group_id = $auth_group->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different realm. In this iteration, this
                // only affects the database meta data and doesn't perform any
                // infrastructure changes.

            }
        }

        // Get Cloud Account relationship
        if(!empty($request_data['cloud_account_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_account_id != $request_data['cloud_account_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_account = Models\Cloud\CloudAccount::query()
                    ->where('id', $request_data['cloud_account_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_account_id = $cloud_account->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different billing account. In this
                // iteration, this only affects the database meta data and doesn't
                // perform any infrastructure changes.

            }
        }

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            // If request value is different than record existing value
            if($record->expires_at != $request_data['expires_at']) {

                $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

                if($request_data['expires_at'] == null) {
                    $record->expires_at = null;
                } elseif($expires_at > now()) {
                    $record->expires_at = $expires_at;
                } elseif($expires_at <= now()) {
                    abort(400, 'The `expires_at` value for the group role cannot be in the past.');
                }

            }
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
