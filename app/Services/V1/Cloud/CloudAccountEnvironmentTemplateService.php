<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class CloudAccountEnvironmentTemplateService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudAccountEnvironmentTemplate::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      cloud_provider_id       required|uuid|exists:cloud_providers,id
     *      api_project_id          required|digits_between:1,20
     *      name                    nullable|string|max:55
     *      description             nullable|string|max:255
     *      display_order           nullable|integer|between:1,99
     *      state                   nullable|string|in:pending,active,inactive,archived,not-found
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get Cloud Provider relationship
        if(!empty($request_data['cloud_provider_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('id', $request_data['cloud_provider_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_provider_id = $cloud_provider->id;
        }

        // Text fields
        $record->git_project_id = Arr::get($request_data, 'git_project_id');
        $record->name = Arr::get($request_data, 'name');
        $record->description = Arr::get($request_data, 'description');
        $record->display_order = Arr::get($request_data, 'display_order', 50);

        $record->save();

        // Update state after model is saved to override BaseModel behavior
        $record->state = Arr::get($request_data, 'state', 'pending');
        $record->save();

        // Use service method to get Git metadata, name, and description
        $this->updateGitMetadata($record->id);

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      name                    nullable|string|max:55
     *      description             nullable|string|max:255
     *      display_order           nullable|integer|between:1,99
     *      state                   nullable|string|in:active,inactive,archived
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->description = Arr::get($request_data, 'description', $record->description);
        $record->display_order = Arr::get($request_data, 'display_order', $record->display_order);

        $record->save();

        // Update state
        if(!empty($request_data['state'])) {

            if($record->state == 'pending' && $request_data['state'] == 'active') {
                $this->updateGitMetadata($record->id);
            } elseif($record->state == 'not-found' && $request_data['state'] == 'active') {
                $this->updateGitMetadata($record->id);
            } elseif($record->state == 'active' && $request_data['state'] == 'inactive') {
                $record->state = 'inactive';
            } elseif($record->state == 'active' && $request_data['state'] == 'archived') {
                $record->state = 'archived';
            } elseif($record->state == 'inactive' && $request_data['state'] == 'active') {
                $this->updateGitMetadata($record->id);
            } elseif($record->state == 'inactive' && $request_data['state'] == 'archived') {
                // TODO Add method to archive GitLab project via API
                $record->state = 'archived';
            } elseif($record->state == 'archived' && $request_data['state'] == 'active') {
                // TODO Add method to unarchive GitLab project via API
                $this->updateGitMetadata($record->id);
            } elseif($record->state == 'archived' && $request_data['state'] == 'inactive') {
                // TODO Add method to unarchive GitLab project via API
                $this->updateGitMetadata($record->id);
                $record->state = 'inactive';
            }

            $record->save();

        }

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        return $record;
    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    /**
     *   Update Git Metadata using Git Provider API credentials in AuthTenant
     *
     *   @param   uuid  $id  UUID of CloudAccountEnvironmentTemplate
     *
     *   @return  bool       True if successful, false if unable to update
     */
    public function updateGitMetadata($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'authTenant'
            ])->where('id', $id)
            ->firstOrFail();

        // If git_project_id field is empty, abort lookup
        if($record->git_project_id == null) {

            $record->state = 'not-found';
            $record->save();
            return false;
        }

        // If api_enabled is false, abort provisioning
        if(config('hackystack.auth.git.providers.gitlab.api_enabled') == false) {
            // TODO Log Git API not enabled
            return false;
        }

        // If AuthTenant Git credentials are empty, abort provisioning
        if($record->authTenant->git_provider_base_url == null || $record->authTenant->git_provider_api_token == null) {
            // TODO Log Git API credentials not configured in AuthTenant model
            abort(501, 'The Git API credentials have not been configured in the AuthTenant. No provisioning has occured.');
            return false;
        }

        // Define Git API credentials from AuthTenant
        $git_provider_type = $record->authTenant->git_provider_type;
        $git_provider_base_url = $record->authTenant->git_provider_base_url;
        $git_provider_api_token = decrypt($record->authTenant->git_provider_api_token);

        // Perform API call to create user in GitLab instance
        if($git_provider_type == 'gitlab') {

            //Perform API call
            try {

                // Utilize HTTP to run a GET request
                $response = Http::withToken($git_provider_api_token)
                    ->get($git_provider_base_url.'/api/v4/projects/'.$record->git_project_id);

                // Throw an exception if a client or server error occured.
                $response->throw();

            } catch(\Illuminate\Http\Client\RequestException $e) {
                dd($e);
                // TODO: Fix error handling to give us useful logs and outputs for debugging.
            }

        }

        $record->git_meta_data = json_decode($response->body());
        $record->git_name = $record->git_meta_data['name'];
        $record->git_description = $record->git_meta_data['description'];
        $record->state = 'active';

        $record->save();

        return true;

    }

}
