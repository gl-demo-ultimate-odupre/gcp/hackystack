<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class HackystackReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hackystack:reset
                            {--D|force-dump-database}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform a factory reset of the HackyStack application and database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        system('clear');
        $this->line('<fg=cyan>  _   _               _            ____   _                _     </>');
        $this->line('<fg=cyan> | | | |  __ _   ___ | | __ _   _ / ___| | |_  __ _   ___ | | __ </>');
        $this->line('<fg=cyan> | |_| | / _` | / __|| |/ /| | | |\___ \ | __|/ _` | / __|| |/ / </>');
        $this->line('<fg=cyan> |  _  || (_| || (__ |   < | |_| | ___) || |_| (_| || (__ |   <  </>');
        $this->line('<fg=cyan> |_| |_| \__,_| \___||_|\_\ \__, ||____/  \__|\__,_| \___||_|\_\ </>');
        $this->line('<fg=cyan>                            |___/                                </>');
        $this->line('');

        $this->line('<fg=cyan;options=bold>Resetting HackyStack to factory defaults...</>');

        // Call the method to perform a backup of configuration files
        $this->call('hackystack:backup');

        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Resetting MySQL database                                                      </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');

        // Create query to database to verify that connection exists
        try {
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $existing_database = DB::select($query, [config('database.connections.mysql.database')]);
            $this->line('<fg=green>Database connection successful.</>');
            $this->line('');
            if($this->option('force-dump-database')) {
                // Drop all tables and re-run all migrations
                $this->call('db:wipe');
            }
            elseif($this->confirm('Are you sure you want to delete all tables in your MySQL database? All data will be lost.')) {
                // Drop all tables and re-run all migrations
                $this->call('db:wipe');
            } else {
                $this->error('You aborted the reset of the database. No additional reset commands will be run.');
                die();
            }
        } catch(\Illuminate\Database\QueryException $e) {
            $this->line('<fg=red>'.$e->getMessage().'</>');
            $this->line('');
            $this->error('Database reset skipped.');
        }

        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Deleting configuration files                                                  </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');

        // Validate that .env file exists
        if(is_file(base_path('.env'))) {
            $this->comment('Deleting `.env` file.');
            File::delete(base_path('.env'));
        } else {
            $this->line('No environment configuration exists at `.env`');
        }

        // Validate that config/hackystack.php file exists
        if(is_file(config_path('hackystack.php'))) {
            $this->comment('Restoring `config/hackystack.php` file to defaults.');
            File::delete(config_path('hackystack.php'));
            File::copy(config_path('hackystack.php.example'), config_path('hackystack.php'));
        } else {
            $this->line('No configuration file exists at `config/hackystack.php`');
        }

        // Copy hackystack configuration file example
        //$this->comment('Copying `config/hackystack.php.example` to `config/hackystack.php`.');
        //File::copy(config_path('hackystack.php.example'), config_path('hackystack.php'));

        // Confirmation message
        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=green;options=bold>HackyStack has been reset to factory defaults.</>');
        $this->line('Please run `php hacky hackystack:install` to perform the initial configuration of HackyStack.');
        $this->line('');

    }
}
