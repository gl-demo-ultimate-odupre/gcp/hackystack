<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthGroupUserGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group-user:get
                            {short_id? : The short ID of the group user.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Group User by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-group-user:get a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_group_user = Models\Auth\AuthGroupUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_group_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Auth Group User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_group_user->id
                ],
                [
                    'short_id',
                    $auth_group_user->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_group_user->authTenant->short_id.'] '.$auth_group_user->authTenant->slug
                ],
                [
                    '[relationship] authGroup',
                    '['.$auth_group_user->authGroup->short_id.'] '.$auth_group_user->authGroup->slug
                ],
                [
                    '[relationship] authUser',
                    '['.$auth_group_user->authUser->short_id.'] '.$auth_group_user->authUser->full_name.' <'.$auth_group_user->authUser->email.'>'
                ],
                [
                    'created_at',
                    $auth_group_user->created_at->toIso8601String()
                ],
                [
                    'expires_at',
                    $auth_group_user->expires_at ? $auth_group_user->expires_at->toIso8601String() : 'never'
                ],
            ]
        );

        $this->comment('');

    }
}
