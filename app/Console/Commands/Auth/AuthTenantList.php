<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthTenantList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-tenant:list
                            {--search=* : Wildcard search of name, slug, or description.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Tenants';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Auth\AuthTenant::query()
            ->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                        $query->orWhere('description', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get(['short_id', 'name', 'slug', 'created_at'])->toArray();

        $this->comment('');
        $this->comment('Auth Tenants - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Name', 'Slug', 'Created at'];
            $this->table($headers, $records);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
