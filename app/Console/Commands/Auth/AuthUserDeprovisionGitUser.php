<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthUserDeprovisionGitUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:deprovision-git-user
                            {short_id? : The short ID of the user.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deprovision the Git User account on the AuthTenant Git Provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-user:deprovision-git-user a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_user = Models\Auth\AuthUser::with([
                    'authTenant'
                ])
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Initialize service
        $authUserService = new Services\V1\Auth\AuthUserService();

        $authUserService->deprovisionGitUser($auth_user->id);

        // Refresh the Eloquent model variable with the latest values
        $auth_user = $auth_user->fresh();

        // Auth User Values
        $this->comment('');
        $this->comment('Auth User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $auth_user->short_id
                ],
                [
                    'auth_tenant',
                    '['.$auth_user->authTenant->short_id.'] '.$auth_user->authTenant->slug,
                ],
                [
                    'full_name',
                    $auth_user->full_name
                ],
                [
                    'email',
                    $auth_user->email
                ],
                [
                    'git_user_email',
                    $auth_user->user_handle.'+'.Str::slug(config('hackystack.app.name')).'-gitops-'.$auth_user->short_id.'@'.$auth_user->user_email_domain
                ],
                [
                    'git_provider_base_url',
                    $auth_user->authTenant->git_provider_base_url != null ? '<fg=green>'.$auth_user->authTenant->git_provider_base_url.'</>' : '<fg=red>Not configured yet. Use `auth-tenant:edit '.$auth_user->authTenant->short_id.'`</>'
                ],
                [
                    'git_username',
                    $auth_user->git_username ? '<fg=green>'.$auth_user->git_username.'</>' : '<fg=red>Not generated yet. Use `auth-user:provision-git-user '.$auth_user->short_id.'`</>'
                ],
                [
                    'git_meta_data',
                    $auth_user->git_meta_data ? Str::limit(json_encode($auth_user->git_meta_data), 80, '...') : '<fg=red>Not provisioned yet. Use `auth-user:provision-git-user '.$auth_user->short_id.'`</>'
                ],
            ]
        );

        $this->newLine();
        $this->comment('Deprovisioning completed successfully.');
        $this->newLine();

    }
}
