<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthProviderFieldGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-field:get
                            {short_id? : The short ID of the provider field.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Provider Field by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->line('You can get a list of provider fields using `auth-provider-field:get`');
            $this->line('You can get the record using the short ID using `auth-provider-field:delete a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_provider_field = Models\Auth\AuthProviderField::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_provider_field == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Provider Field Values
        $this->comment('');
        $this->comment('Auth Provider Field');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_provider_field->id
                ],
                [
                    'short_id',
                    $auth_provider_field->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_provider_field->authTenant->short_id.'] '.$auth_provider_field->authTenant->slug
                ],
                [
                    '[relationship] authProvider',
                    '['.$auth_provider_field->authProvider->short_id.'] '.$auth_provider_field->authProvider->slug
                ],
                [
                    'provider_key',
                    $auth_provider_field->provider_key
                ],
                [
                    'user_column',
                    $auth_provider_field->user_column
                ],
                [
                    'created_at',
                    $auth_provider_field->created_at->toIso8601String()
                ],
            ]
        );

        $this->comment('');

    }
}
