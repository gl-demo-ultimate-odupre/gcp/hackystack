<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class AuthGroupCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group:create
                            {--T|auth_tenant_slug= : The slug of the tenant this group belongs to}
                            {--N|name= : The display name of this group}
                            {--S|slug= : The alpha-dash shorthand name for this group}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Authentication Group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Groups - Create Record');

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this group belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this group?');
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this group?');
        }

        // Verify inputs table
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'auth_tenant_id',
                    '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                ],
                [
                    'name',
                    $name
                ],
                [
                    'slug',
                    $slug
                ],
            ]
        );

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the creation of the record?')) {
            $this->error('Error: You aborted. No record was created.');
            die();
        }

        // Initialize service
        $authGroupService = new Services\V1\Auth\AuthGroupService();

        // Use service to create record
        $record = $authGroupService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'name' => $name,
            'slug' => $slug,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('auth-group:get', [
            'short_id' => $record->short_id
        ]);

    }

}
