<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudRealmDeprovision extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-realm:deprovision
                            {short_id? : The short ID of the realm.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deprovision a cloud realm in the cloud provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of cloud realms using `cloud-realm:list`');
            $this->line('You can lookup by short ID using `cloud-realm:provision a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_realm == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }
/*
        // Validate that cloud realm does not have API meta data
        if(!$cloud_realm->api_meta_data) {
            $this->error('This realm does not have API meta data and is assumed to not be provisioned.');
            $this->line('You can run the `cloud-realm:provision '.$cloud_realm->short_id.'` command to provision infrastructure resources.');
            $this->error('');
            die();
        }
*/
        // Call the get method to display the tables of values for the record.
        $this->call('cloud-realm:get', [
            'short_id' => $cloud_realm->short_id,
            '--without-child-relationships' => true
        ]);

        $this->comment('');

        // Initialize service
        $cloudRealmService = new Services\V1\Cloud\CloudRealmService();

        // Use service to provision using API
        $cloudRealmService->deprovisionCloudProviderOrganizationUnit($cloud_realm->id);

        // Get refreshed cloud realm with updated meta data
        $cloud_realm = $cloud_realm->fresh();

        //if($cloud_realm->provisioned_at == null) {
            //$this->line('<fg=green>Realm deprovisioning complete.</>');
        //} else {
            //$this->line('<fg=red>Realm deprovisioning failed.</>');
        //}

    }
}
