<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class CloudAccountEnvironmentList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment:list
                            {--T|auth_tenant_slug= : The slug of the tenant that the environment templates belong to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that environment templates belongs to}
                            {--C|cloud_account_slug= : If set, results will be filtered to a specific Cloud Account.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Account Environments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get cloud provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // Get cloud account slug
        $cloud_account_slug = $this->option('cloud_account_slug');

        // Get list of records from database
        $records = Models\Cloud\CloudAccountEnvironment::query()
            ->with([
                'authTenant',
                'cloudProvider',
                'cloudAccount'
            ])->where(function ($query) use ($auth_tenant_slug) {
                if ($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function ($query) use ($cloud_provider_slug) {
                if ($cloud_provider_slug) {
                    $query->whereHas('cloudProvider', function (Builder $query) use ($cloud_provider_slug) {
                        $query->where('slug', $cloud_provider_slug);
                    });
                }
            })->where(function ($query) use ($cloud_account_slug) {
                if ($cloud_account_slug) {
                    $query->whereHas('cloudAccount', function (Builder $query) use ($cloud_account_slug) {
                        $query->where('slug', $cloud_account_slug);
                    });
                }
            })->orderBy('name')
            ->get();

        // Loop through records in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach ($records as $environment_record) {
            $environment_rows[] = [
                'short_id' => $environment_record->short_id,
                'auth_tenant' => '['.$environment_record->authTenant->short_id.'] '.$environment_record->authTenant->slug,
                'cloud_provider' => '['.$environment_record->cloudProvider->short_id.'] '.$environment_record->cloudProvider->slug,
                'cloud_account' => '['.$environment_record->cloudAccount->short_id.'] '.$environment_record->cloudAccount->slug,
                'git_project_id' => $environment_record->git_meta_data ? $environment_record->git_meta_data['id'] : 'not provisioned',
                'name' => $environment_record->name,
                'state' => $environment_record->state,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Environment - List of Records');

        if (count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Cloud Account', 'Git Project ID', 'Name', 'State'];
            $this->table($headers, $environment_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');
        } else {
            $this->line('No records exist.');
            $this->comment('');
        }
    }
}
