<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountProvisionGcpBillingAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account:provision-gcp-billing-account
                            {short_id? : The short ID of the cloud account.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provision the (updated) GCP Billing Account for an existing Cloud Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of cloud accounts using `cloud-account:list`');
            $this->line('You can provision the billing account using `cloud-account:provision-gcp-billing-account a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Initialize service
        $googleCloudBillingOrganizationService = new Services\V1\Vendor\Gcp\CloudBillingOrganizationService($cloud_account->cloud_provider_id);
        $googleCloudBillingProjectService = new Services\V1\Vendor\Gcp\CloudBillingProjectService($cloud_account->cloud_provider_id);
        $cloudAccountService = new Services\V1\Cloud\CloudAccountService();

        // Get details about current project billing account configuration
        $project_billing_account = $googleCloudBillingProjectService->get(['project_name' => $cloud_account->api_meta_data['name']]);

        $project_billing_account_values = [];
        foreach($project_billing_account as $key => $value) {
            $project_billing_account_values[] = [
                $key,
                is_array($value) ? json_encode($value) : $value
            ];
        }

        $this->line('');
        $this->comment('Current Project Billing Account');
        $this->table(
            ['Column', 'API Value'],
            $project_billing_account_values
        );

        // Check if billing account has already been configured
        if($project_billing_account->billingAccountName == $cloud_account->cloudBillingAccount->api_meta_data['name']) {
            $this->line('');
            $this->error('The billing account for this project has already been provisioned and matches the Cloud Billing Account.');
            $this->line('To modify the billing account, use `cloud-billing-account:edit '.$cloud_account->cloudBillingAccount->short_id.'`');
            $this->error('');
            die();
        }

        // Get details about organization billing account using GCP API
        $organization_billing_account = $googleCloudBillingOrganizationService->get(['billing_account_name' => $cloud_account->cloudBillingAccount->api_meta_data['name']]);

        $organization_billing_account_values = [];
        foreach($organization_billing_account as $key => $value) {
            $organization_billing_account_values[] = [
                $key,
                is_array($value) ? json_encode($value) : $value
            ];
        }

        $this->line('');
        $this->comment('Organization Billing Account');
        $this->table(
            ['Column', 'API Value'],
            $organization_billing_account_values
        );

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the update of the Cloud Account to the Organization Billing Account?')) {
            $this->error('Error: You aborted. The billing account has not been updated.');
            die();
        }

        // Use service to delete record
        $cloudAccountService->provisionBillingAccount($cloud_account->id);

        sleep(1);

        // Get details about updated project billing account configuration
        $project_billing_account = $googleCloudBillingProjectService->get(['project_name' => $cloud_account->api_meta_data['name']]);

        $project_billing_account_values = [];
        foreach($project_billing_account as $key => $value) {
            $project_billing_account_values[] = [
                $key,
                is_array($value) ? json_encode($value) : $value
            ];
        }

        $this->line('');
        $this->comment('Updated Project Billing Account');
        $this->table(
            ['Column', 'API Value'],
            $project_billing_account_values
        );
        $this->line('');

        // Show result in console
        $this->comment('Billing account successfully updated.');
        $this->comment('');

    }
}
