<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CloudAccountEnvironmentRotateKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment:rotate-key
                            {short_id? : The short ID of the environment.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rotate the GCP Service Account Key for a Cloud Account Environment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of accounts using `cloud-account-environment:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_environment = Models\Cloud\CloudAccountEnvironment::with([
                    'authTenant',
                    'cloudProvider',
                    // 'cloudAccountEnvironments'
                ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account_environment == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account Values
        $this->comment('');
        $this->comment('Cloud Account Environment - ' . $cloud_account_environment->short_id . ' - ' . $cloud_account_environment->name);

        if($cloud_account_environment->git_meta_data != null) {
            $cloudAccountEnvironmentService = new \App\Services\V1\Cloud\CloudAccountEnvironmentService();
            $result = $cloudAccountEnvironmentService->rotateGitOpsServiceAccountKey($cloud_account_environment->id);

            if($result == true) {
                $this->line('<fg=green>The key has been rotated.</>');
            } else {
                $this->line('<fg=red>There was an error rotating the key.</>');
            }
        } else {
            $this->line('<fg=red>Git Meta Data is empty.</>');
        }
    }
}
