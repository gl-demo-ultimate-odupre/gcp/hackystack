<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountRoleGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-role:get
                            {short_id? : The short ID of the cloud account role.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Account Role by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->comment('You can lookup by short ID using `cloud-account-role:get a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_role = Models\Cloud\CloudAccountRole::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account_role == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account Role Values
        $this->comment('');
        $this->comment('Cloud Account Role');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account_role->id
                ],
                [
                    'short_id',
                    $cloud_account_role->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$cloud_account_role->authTenant->short_id.'] '.$cloud_account_role->authTenant->slug
                ],
                [
                    '[relationship] cloudProvider',
                    '['.$cloud_account_role->cloudProvider->short_id.'] '.$cloud_account_role->cloudProvider->slug
                ],
                [
                    '[relationship] cloudAccount',
                    '['.$cloud_account_role->cloudAccount->short_id.'] '.$cloud_account_role->cloudAccount->slug
                ],
                [
                    '[relationship] cloudRealm',
                    '['.$cloud_account_role->cloudRealm->short_id.'] '.$cloud_account_role->cloudRealm->slug
                ],
                [
                    'api_name',
                    $cloud_account_role->api_name
                ],
                [
                    'created_at',
                    $cloud_account_role->created_at->toIso8601String()
                ],
                [
                    'created_by',
                    $cloud_account_role->created_by ? '['.$cloud_account_role->createdBy->short_id.'] '.$cloud_account_role->createdBy->full_name : 'System',
                ],
                [
                    'provisioned_at',
                    $cloud_account_role->provisioned_at ? $cloud_account_role->provisioned_at->toIso8601String() : 'not provisioned'
                ],
                [
                    'state',
                    $cloud_account_role->state
                ],
            ]
        );

        // If api_meta_data database field is not empty
        if($cloud_account_role->api_meta_data) {

            $api_meta_data_values = [];
            foreach($cloud_account_role->api_meta_data as $meta_data_key => $meta_data_value) {
                $api_meta_data_values[] = [
                    $meta_data_key,
                    is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
                ];
            }

            $this->table(
                ['API Meta Data Column', 'API Value'],
                $api_meta_data_values
            );

        }


        //
        // Cloud Account - Child Relationship - Cloud Account Users
        // --------------------------------------------------------------------
        // Loop through cloud account users in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through records and add values to array
        $cloud_account_user_rows = [];
        foreach($cloud_account_role->cloudAccountUsers as $user) {
            $cloud_account_user_rows[] = [
                'short_id' => $user->short_id,
                'auth_user_short_id' => $user->authUser->short_id,
                'username' => $user->username,
                'full_name' => $user->authUser->full_name,
                'email' => $user->authUser->email,
                'created_at' => $user->created_at->toIso8601String(),
                'provisioned_at' => $user->provisioned_at->toIso8601String(),
                'state' => $user->state
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Users');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_user_rows) > 0) {
            $this->table(
                ['User Short ID', 'Auth User Short ID', 'Username', 'Full Name', 'Email', 'Created at', 'Provisioned at', 'State'],
                $cloud_account_user_rows
            );
        } else {
            $this->error('No cloud account users have been attached to this role.');
        }

        $this->comment('');

    }
}
