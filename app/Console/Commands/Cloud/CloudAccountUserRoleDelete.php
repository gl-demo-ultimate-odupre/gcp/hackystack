<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountUserRoleDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user-role:delete
                            {short_id? : The short ID of the user role.}
                            {--R|cloud_account_role_short_id= : The short ID of the Cloud Account Role}
                            {--U|cloud_account_user_short_id= : The short ID of the Cloud Account User}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Detach a Cloud Account User from a Role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Get short ID
        $short_id = $this->argument('short_id');

        // Get relationship IDs
        $cloud_account_user_short_id = $this->option('cloud_account_user_short_id');
        $cloud_account_role_short_id = $this->option('cloud_account_role_short_id');

        // If short ID or optional relationship is not set, return an error message
        if ($short_id == null && $cloud_account_user_short_id == null) {
            $this->error('You did not specify the short_id or relationship IDs to lookup the record.');
            $this->comment('You can delete by short ID using `cloud-account-user-role:delete a1b2c3d4`');
            $this->comment('You can delete by relationship ID using `cloud-account-user-role:delete --cloud_account_role_short_id=e5f6g7h8 --cloud_account_user_short_id=i9j0k1l2`');
            $this->comment('You can get a list of cloud account users and cloud account roles using `cloud-account:get a1b2c3d4`');
            $this->comment('You can get a list of cloud accounts using `cloud-account:list`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif ($short_id != null) {
            $cloud_account_user_role = Models\Cloud\CloudAccountUserRole::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If relationship IDs are specified
        elseif ($cloud_account_user_short_id != null) {
            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('short_id', $cloud_account_user_short_id)
                ->first();

            // If record not found, return an error message
            if ($cloud_account_user == null) {
                $this->error('No cloud account user was found with that short ID.');
                $this->error('');
                die();
            }

            $cloud_account_role = Models\Cloud\CloudAccountRole::query()
                ->where('short_id', $cloud_account_role_short_id)
                ->first();

            // If record not found, return an error message
            if ($cloud_account_role == null) {
                $this->error('No cloud account role was found with that short ID.');
                $this->error('');
                die();
            }

            $cloud_account_user_role = Models\Cloud\CloudAccountUserRole::query()
                ->where('cloud_account_user_id', $cloud_account_user->id)
                ->where('cloud_account_role_id', $cloud_account_role->id)
                ->first();
        }

        // If record not found, return an error message
        if ($cloud_account_user_role == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Ask for confirmation to abort creation.
        if ($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudAccountUserRoleService = new Services\V1\Cloud\CloudAccountUserRoleService();

        // Use service to delete record
        $cloudAccountUserRoleService->destroy($cloud_account_user_role->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');
    }
}
