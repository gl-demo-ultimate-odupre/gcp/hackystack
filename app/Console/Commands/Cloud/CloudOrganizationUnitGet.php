<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudOrganizationUnitGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-organization-unit:get
                            {short_id? : The short ID of the organization unit.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Organization Unit by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of organization units using `cloud-organization-unit:list`');
            $this->line('You can lookup by short ID using `cloud-organization-unit:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_organization_unit == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Cloud Organization Unit');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_organization_unit->id
                ],
                [
                    'short_id',
                    $cloud_organization_unit->short_id
                ],
                [
                    'auth_tenant_id',
                    $cloud_organization_unit->auth_tenant_id
                ],
                [
                    'cloud_organization_unit_id_parent',
                    $cloud_organization_unit->cloud_organization_unit_id_parent
                ],
                [
                    'cloud_provider_id',
                    $cloud_organization_unit->cloud_provider_id
                ],
                [
                    'cloud_realm_id',
                    $cloud_organization_unit->cloud_realm_id
                ],
                [
                    'name',
                    $cloud_organization_unit->name
                ],
                [
                    'slug',
                    $cloud_organization_unit->slug
                ],
                [
                    'created_at',
                    $cloud_organization_unit->created_at->toIso8601String()
                ],
            ]
        );

        if($cloud_organization_unit->cloud_organization_unit_id_parent) {

            // Parent Cloud Organization Unit Values
            $this->comment('');
            $this->comment('Parent Cloud Organization Unit');
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'short_id',
                        $cloud_organization_unit->parentCloudOrganizationUnit->short_id
                    ],
                    [
                        'parent_cloud_organization_unit',
                        $cloud_organization_unit->parentCloudOrganizationUnit->cloud_organization_unit_id_parent ? '['.$cloud_organization_unit->parentCloudOrganizationUnit->parentCloudOrganizationUnit->short_id.'] '.$cloud_organization_unit->parentCloudOrganizationUnit->parentCloudOrganizationUnit->slug : ''
                    ],
                    [
                        'name',
                        $cloud_organization_unit->parentCloudOrganizationUnit->name
                    ],
                    [
                        'slug',
                        $cloud_organization_unit->parentCloudOrganizationUnit->slug
                    ],
                ]
            );

        }

        // Cloud Realm Values
        $this->comment('');
        $this->comment('Cloud Realm');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $cloud_organization_unit->cloudRealm->short_id
                ],
                [
                    'name',
                    $cloud_organization_unit->cloudRealm->name
                ],
                [
                    'slug',
                    $cloud_organization_unit->cloudRealm->slug
                ],
            ]
        );

        // Cloud Provider Values
        $this->comment('');
        $this->comment('Cloud Provider');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $cloud_organization_unit->cloudProvider->short_id
                ],
                [
                    'name',
                    $cloud_organization_unit->cloudProvider->name
                ],
                [
                    'slug',
                    $cloud_organization_unit->cloudProvider->slug
                ],
            ]
        );

        //
        // Cloud Organization Unit - Child Relationship - Organization Units
        // --------------------------------------------------------------------
        // Loop through child organization units in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through organization units and add values to array
        $organization_unit_rows = [];
        foreach($cloud_organization_unit->childCloudOrganizationUnits as $unit) {
            $organization_unit_rows[] = [
                'unit_short_id' => $unit->short_id,
                'unit_name' => $unit->name,
                'unit_slug' => $unit->slug,
            ];
        }

        $this->comment('');
        $this->comment('Child Cloud Organization Units');

        // If rows exists, render a table or return a no results found message
        if(count($organization_unit_rows) > 0) {
            $this->table(
                ['Organization Unit Short ID', 'Unit Name', 'Unit Slug'],
                $organization_unit_rows
            );
        } else {
            $this->error('No child organization units have been attached.');
        }

        //
        // Cloud Billing Account - Child Relationship - Cloud Account
        // --------------------------------------------------------------------
        // Loop through accounts in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        //

        // Loop through users and add values to array
        $account_rows = [];
        foreach($cloud_organization_unit->cloudAccounts as $account) {
            $account_rows[] = [
                'account_short_id' => $account->short_id,
                'cloud_realm' => '['.$account->cloudRealm->short_id.'] '.$account->cloudRealm->slug,
                'name' => $account->name,
                'slug' => $account->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Accounts');

        // If rows exists, render a table or return a no results found message
        if(count($account_rows) > 0 && count($account_rows) < 100) {
            $this->table(
                ['Account Short ID', 'Cloud Realm', 'Name', 'Slug'],
                $account_rows
            );
        } elseif(count($account_rows) > 100) {
            $this->comment('There are '.count($account_rows).' cloud accounts attached to this organization unit.');
        } else {
            $this->error('No cloud accounts have been attached.');
        }

        $this->comment('');

    }
}
