<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudAccountCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account:create
                            {--T|auth_tenant_slug= : The slug of the tenant this cloud account belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider this cloud account belongs to}
                            {--R|cloud_realm_slug= : The slug of the cloud realm this cloud account belongs to}
                            {--O|cloud_organization_unit_slug= : The slug of the cloud organization unit that this cloud account is nested in}
                            {--N|name= : The display name of this cloud account}
                            {--S|slug= : The alpha-dash shorthand name for this cloud account}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Cloud Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Accounts - Create Record');

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this cloud account belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this cloud account belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Realm
        // --------------------------------------------------------------------
        //

        // Get realm slug
        $cloud_realm_slug = $this->option('cloud_realm_slug');

        // If realm slug option was specified, lookup by slug
        if($cloud_realm_slug) {
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_realm_slug)
                ->first();
        }

        // If realm slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_realms = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->get(['slug'])
                ->toArray();

            // If no billing accounts exist, then return an error
            if(count($cloud_realms) == 0) {
                $this->error('Error: No cloud realms were found for the tenant and provider.');
                $this->comment('Please create a cloud realm before creating a cloud account.');
                $this->line('cloud-realm:create');
                $this->error('');
                die();
            }

            $this->line('');
            $this->line('Available cloud realms: '.implode(',', Arr::flatten($cloud_realms)));

            $cloud_realm_prompt = $this->anticipate('Which cloud realm should this cloud account belong to?', Arr::flatten($cloud_realms));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_realm_prompt)
                ->firstOrFail();

        }

        // Validate that realm exists or return error message
        if(!$cloud_realm) {
            $this->error('Error: No cloud realm was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Parent Cloud Organization Unit
        // --------------------------------------------------------------------
        //

        // Get realm slug
        $cloud_organization_unit_slug = $this->option('cloud_organization_unit_slug');

        // If realm slug option was specified, lookup by slug
        if($cloud_organization_unit_slug) {
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_realm_id', $cloud_realm->id)
                ->where('slug', $cloud_organization_unit_slug)
                ->first();
        }

        // If organization unit slug was not provided, prompt for input
        else {

            // Get list of organization unit to show in console
            $cloud_organization_units = Models\Cloud\CloudOrganizationUnit::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_realm_id', $cloud_realm->id)
                ->get(['slug'])
                ->toArray();

            // Available cloud organization units
            $this->table(
                ['Organization Unit Slug'],
                $cloud_organization_units
            );

            $cloud_organization_unit_prompt = $this->anticipate('Which cloud organization unit should this cloud account belong to?', Arr::flatten($cloud_organization_units));

            // Lookup cloud organization unit based on slug provided in prompt
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_realm_id', $cloud_realm->id)
                ->where('slug', $cloud_organization_unit_prompt)
                ->firstOrFail();

        }

        // Validate that realm exists or return error message
        if(!$cloud_organization_unit) {
            $this->error('Error: No cloud organization unit was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this cloud account?');
        }

        // Get list of example slugs to show in console
        $cloud_account_individual_slugs = Models\Cloud\CloudAccount::has('cloudAccountUsers', '=', 1)->take(2)->get(['slug'])->toArray();
        $cloud_account_group_slugs = Models\Cloud\CloudAccount::has('cloudAccountUsers', '>', 2)->take(2)->get(['slug'])->toArray();

        $this->comment('The slug should include any naming standards prefix.');
        $this->line('Example individual account slugs: '.implode(',', Arr::flatten($cloud_account_individual_slugs)));
        $this->line('Example group account slugs: '.implode(',', Arr::flatten($cloud_account_group_slugs)));

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this cloud account?');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'cloud_provider_id',
                        '['.$cloud_provider->short_id.'] '.$cloud_provider->slug
                    ],
                    [
                        'cloud_realm_id',
                        '['.$cloud_realm->short_id.'] '.$cloud_realm->slug
                    ],
                    [
                        'cloud_organization_unit_id',
                        '['.$cloud_organization_unit->short_id.'] '.$cloud_organization_unit->slug
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'slug',
                        $slug
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $cloudAccountService = new Services\V1\Cloud\CloudAccountService();

        // Use service to create record
        $record = $cloudAccountService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_billing_account_id' => $cloud_realm->cloud_billing_account_id,
            'cloud_organization_unit_id' => $cloud_organization_unit->id,
            'cloud_provider_id' => $cloud_provider->id,
            'cloud_realm_id' => $cloud_realm->id,
            'name' => $name,
            'slug' => $slug,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-account:get', [
            'short_id' => $record->short_id
        ]);

    }

}
