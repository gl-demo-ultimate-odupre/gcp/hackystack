<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use App\Services\V1\Vendor\Aws\OrganizationAccountService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudAccountUserRoleScheduledProvisioning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user-role:scheduled-provisioning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provision all Cloud Account User Roles that have provision_at set and have not been provisioned.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get all cloud account user roles that have not been provisioned
        $cloud_account_user_roles = Models\Cloud\CloudAccountUserRole::with([
            'cloudAccountUser'
            ])->whereHas('cloudAccountUser', function($query) {
                $query->where('flag_provisioned', true);
            })->where('flag_provisioned', false)
            ->where('provision_at', '<', now())
            ->where('state', 'provisioning-pending')
            ->get();

        // Loop through AWS account users
        foreach($cloud_account_user_roles as $cloud_account_user_role) {

            $cloudAccountUserRoleService = new Services\V1\Cloud\CloudAccountUserRoleService();

            // Provision cloud account user
            $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$cloud_account_user_role->id.'] Provisioning Cloud Account User Role');
            $cloudAccountUserRoleService->provision($cloud_account_user_role->id);

        }

    }

}
