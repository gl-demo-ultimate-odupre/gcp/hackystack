<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountEnvironmentTemplateDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:delete
                            {short_id? : The short ID of the environment template.}
                            {--force : Force deletion of environment template without warning about child relationships.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Environment Template by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of environment templates using `cloud-account-environment-template:list`');
            $this->line('You can delete by short ID using `cloud-account-environment-template:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account_environment_template == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If cloud account environments are attached to this realm, abort deletion
        if($cloud_account_environment_template->cloud_accounts_count > 0 && $this->option('force') == null) {
            $this->error('Error: You cannot delete a environment template that has cloud accounts attached.');
            $this->line('You can get a list of cloud account environments associated with this environment template using `cloud-account-environment-template:get '.$cloud_account_environment_template->short_id.'`');
            $this->error('');
            die();
        }

        // Cloud Account Environment Template Values
        $this->comment('');
        $this->comment('Cloud Account Environment Template');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $cloud_account_environment_template->short_id
                ],
                [
                    'git_project_id',
                    $cloud_account_environment_template->git_project_id
                ],
                [
                    'name',
                    $cloud_account_environment_template->name
                ],
                [
                    'description',
                    $cloud_account_environment_template->slug
                ],
                [
                    'state',
                    $cloud_account_environment_template->state
                ],
                [
                    'created_at',
                    $cloud_account_environment_template->created_at
                ],
            ]
        );

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudAccountEnvironmentTemplateService = new Services\V1\Cloud\CloudAccountEnvironmentTemplateService();

        // Use service to delete record
        $cloudAccountEnvironmentTemplateService->delete($cloud_account_environment_template->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
