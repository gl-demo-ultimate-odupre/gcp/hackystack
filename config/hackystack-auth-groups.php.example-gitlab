<?php

/**
 *   Auth Groups - Import Configuration
 *
 *   This configuration file is used in conjunction with the CLI command that
 *   imports these auth groups into the database. The array key for each record
 *   should be the same as the slug.
 *
 *   > hacky-cli auth-group:import-config-file -F hackystack-auth-groups
 *
 *   --------------------------------------------------------------------------
 *
 *   'eng' => [
 *      'name' => 'Engineering',
 *      'slug' => 'eng',
 *      'groups' => [
 *          'eng-dev' => [
 *              'name' => 'Development Team',
 *              'slug' => 'eng-dev',
 *          ],
 *          ...
 *      ],
 *   ],
 *   'it' => [
 *      ...
 *   ],
 *
 *   --------------------------------------------------------------------------
 *
 *   If an existing record exists that has an ID that you want to re-use, you
 *   can optionally add the 'id' key to the array before the 'name' key of a
 *   group and set the value to the UUID. When the record is imported or
 *   matched against an existing record, the record will be updated with
 *   the ID specified in this file.
 *
 *   --------------------------------------------------------------------------
 *
 *   'eng' => [
 *      'id' => 'a4b17b79-0e8b-4f3a-a892-03b794aec37b',
 *      'name' => 'Engineering',
 *      'slug' => 'eng',
 *      'groups' => [
 *         ...
 *      ],
 *   ],
 *
*/

return [
    'eng' => [
        'namespace' => 'division',
        'name' => 'Engineering',
        'slug' => 'eng',
        'providers' => [
            'okta' => [
                'default' => false,
                'meta' => [
                    [
                        'meta_key' => 'gl_division',
                        'meta_value' => 'Engineering',
                    ],
                ],
            ],
        ],
        'groups' => [
            'eng-support' => [
                'namespace' => 'department',
                'name' => 'Customer Support',
                'slug' => 'eng-support',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Customer Support',
                            ],
                        ],
                    ],
                ],
            ],
            'eng-dev' => [
                'namespace' => 'department',
                'name' => 'Development',
                'slug' => 'eng-dev',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Development',
                            ],
                        ],
                    ],
                ],
            ],
            'eng-infra' => [
                'namespace' => 'department',
                'name' => 'Infrastructure',
                'slug' => 'eng-infra',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Infrastructure',
                            ],
                        ],
                    ],
                ],
            ],
            'eng-quality' => [
                'namespace' => 'department',
                'name' => 'Quality',
                'slug' => 'eng-quality',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Quality',
                            ],
                        ],
                    ],
                ],
            ],
            'eng-security' => [
                'namespace' => 'department',
                'name' => 'Security',
                'slug' => 'eng-security',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Security',
                            ],
                        ],
                    ],
                ],
            ],
            'eng-ux' => [
                'namespace' => 'department',
                'name' => 'UX',
                'slug' => 'eng-ux',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'UX',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'ga' => [
        'name' => 'GA',
        'slug' => 'ga',
        'namespace' => 'division',
        'providers' => [
            'okta' => [
                'default' => false,
                'meta' => [
                    [
                        'meta_key' => 'gl_division',
                        'meta_value' => 'GA',
                    ],
                ],
            ],
        ],
        'groups' => [
            'ga-accounting' => [
                'namespace' => 'department',
                'name' => 'Accounting',
                'slug' => 'ga-accounting',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Accounting',
                            ],
                        ],
                    ],
                ],
            ],
            'ga-business-ops' => [
                'namespace' => 'department',
                'name' => 'Business Technology',
                'slug' => 'ga-business-tech',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Business Technology',
                            ],
                        ],
                    ],
                ],
            ],
            'ga-ceo' => [
                'namespace' => 'department',
                'name' => 'CEO',
                'slug' => 'ga-ceo',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'CEO',
                            ],
                        ],
                    ],
                ],
            ],
            'ga-finance' => [
                'namespace' => 'department',
                'name' => 'Finance',
                'slug' => 'ga-finance',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Finance',
                            ],
                        ],
                    ],
                ],
            ],
            'ga-legal' => [
                'namespace' => 'department',
                'name' => 'Legal',
                'slug' => 'ga-legal',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Legal',
                            ],
                        ],
                    ],
                ],
            ],
            'ga-people' => [
                'namespace' => 'department',
                'name' => 'People Success',
                'slug' => 'ga-people',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'People Success',
                            ],
                        ],
                    ],
                ],
            ],
            'ga-recruiting' => [
                'namespace' => 'department',
                'name' => 'Recruiting',
                'slug' => 'ga-recruiting',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Recruiting',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'mktg' => [
        'namespace' => 'division',
        'name' => 'Marketing',
        'slug' => 'mktg',
        'providers' => [
            'okta' => [
                'default' => false,
                'meta' => [
                    [
                        'meta_key' => 'gl_division',
                        'meta_value' => 'Marketing',
                    ],
                ],
            ],
        ],
        'groups' => [
            'mktg-awareness' => [
                'namespace' => 'department',
                'name' => 'Awareness',
                'slug' => 'mktg-awareness',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Awareness',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-brand-design' => [
                'namespace' => 'department',
                'name' => 'Brand & Digital Design',
                'slug' => 'mktg-brand-design',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Brand & Digital Design',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-campaigns' => [
                'namespace' => 'department',
                'name' => 'Campaigns',
                'slug' => 'mktg-campaigns',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Campaigns',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-communications' => [
                'namespace' => 'department',
                'name' => 'Communications',
                'slug' => 'mktg-communications',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Communications',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-community' => [
                'namespace' => 'department',
                'name' => 'Community Relations',
                'slug' => 'mktg-community',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Community Relations',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-content' => [
                'namespace' => 'department',
                'name' => 'Content Marketing',
                'slug' => 'mktg-content',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Content Marketing',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-digital' => [
                'namespace' => 'department',
                'name' => 'Digital Marketing',
                'slug' => 'mktg-digital',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Digital Marketing',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-field' => [
                'namespace' => 'department',
                'name' => 'Field Marketing',
                'slug' => 'mktg-field',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Field Marketing',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-inbound' => [
                'namespace' => 'department',
                'name' => 'Inbound Marketing',
                'slug' => 'mktg-inbound',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Inbound Marketing',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-ops' => [
                'namespace' => 'department',
                'name' => 'Marketing Ops',
                'slug' => 'mktg-ops',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Marketing Ops',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-events' => [
                'namespace' => 'department',
                'name' => 'Owned Events',
                'slug' => 'mktg-events',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Owned Events',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-partner' => [
                'namespace' => 'department',
                'name' => 'Partner Marketing',
                'slug' => 'mktg-partner',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Partner Marketing',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-product' => [
                'namespace' => 'department',
                'name' => 'Product Marketing',
                'slug' => 'mktg-product',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Product Marketing',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-sales-dev' => [
                'namespace' => 'department',
                'name' => 'Sales Development',
                'slug' => 'mktg-sales-dev',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Sales Development',
                            ],
                        ],
                    ],
                ],
            ],
            'mktg-strategic' => [
                'namespace' => 'department',
                'name' => 'Strategic Marketing',
                'slug' => 'mktg-strategic',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Strategic Marketing',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'product' => [
        'namespace' => 'division',
        'name' => 'Product',
        'slug' => 'product',
        'providers' => [
            'okta' => [
                'default' => false,
                'meta' => [
                    [
                        'meta_key' => 'gl_division',
                        'meta_value' => 'Product',
                    ],
                ],
            ],
        ],
        'groups' => [
            'product-mgmt' => [
                'namespace' => 'department',
                'name' => 'Product Management',
                'slug' => 'product-mgmt',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Product Management',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'sales' => [
        'namespace' => 'division',
        'name' => 'Sales',
        'slug' => 'sales',
        'providers' => [
            'okta' => [
                'default' => false,
                'meta' => [
                    [
                        'meta_key' => 'gl_division',
                        'meta_value' => 'Sales',
                    ],
                ],
            ],
        ],
        'groups' => [
            'sales-alliances' => [
                'namespace' => 'department',
                'name' => 'Alliances',
                'slug' => 'sales-alliances',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Alliances',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-channel' => [
                'namespace' => 'department',
                'name' => 'Channel',
                'slug' => 'sales-channel',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Channel',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-commercial' => [
                'namespace' => 'department',
                'name' => 'Commercial Sales',
                'slug' => 'sales-commercial',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Commercial Sales',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-cs' => [
                'namespace' => 'department',
                'name' => 'Customer Success',
                'slug' => 'sales-cs',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Customer Success',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-cs-ps-consulting' => [
                'namespace' => 'department',
                'name' => 'Consulting Delivery',
                'slug' => 'sales-cs-ps-consulting',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Consulting Delivery',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-ent' => [
                'namespace' => 'department',
                'name' => 'Enterprise Sales',
                'slug' => 'sales-ent',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Enterprise Sales',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-field-ops' => [
                'namespace' => 'department',
                'name' => 'Field Operations',
                'slug' => 'sales-field-ops',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Field Operations',
                            ],
                        ],
                    ],
                ],
            ],
            'sales-practice-mgmt' => [
                'namespace' => 'department',
                'name' => 'Practice Management',
                'slug' => 'sales-practice-mgmt',
                'providers' => [
                    'okta' => [
                        'default' => false,
                        'meta' => [
                            [
                                'meta_key' => 'gl_department',
                                'meta_value' => 'Practice Management',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
