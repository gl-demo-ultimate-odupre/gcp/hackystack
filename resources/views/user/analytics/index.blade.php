@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="alert alert-warning">
                This is a placeholder for the analytics.
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
