<footer class="footer">
    <div>
        @if(config('hackystack.footer.copyright'))
            {{ config('hackystack.footer.copyright') }}<br />
        @endif
        @if(config('hackystack.footer.support_instructions'))
            {{ config('hackystack.footer.support_instructions') }}<br />
        @endif
        @if(config('hackystack.footer.documentation_link'))
            <a class="mr-1" href="{{ config('hackystack.footer.documentation_link') }}">Documentation</a>|
        @endif
        @if(config('hackystack.footer.support_link'))
            <a class="ml-1" href="{{ config('hackystack.footer.support_link') }}">Support</a>
        @endif
    </div>
</footer>
