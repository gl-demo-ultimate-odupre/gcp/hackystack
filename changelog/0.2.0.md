# 0.2.0
* **URL:** https://gitlab.com/hackystack/hackystack-portal/-/milestones/8
* **Release Date:** 2021-01-28

## Overview
This is the second release of HackyStack that fixes bugs with AWS API usage, provides stability for creating individual AWS accounts, adds features for creating AWS group accounts, and additional backend and CLI features.

### New Features
* AWS Group Accounts - You can create an AWS account using the CLI and create multiple Cloud Account Users for the AWS account that allow multiple users to share the same infrastructure resources.
* Additional Cloud CLI Commands - We have added additional CLI commands for Cloud Account Users, Cloud Account Roles
* Early iteration of Dockerfile for easy installation of HackyStack Portal. This is for development and testing purposes and is not considered stable yet.

### Limitations
* `High` [#38](https://gitlab.com/hackystack/hackystack-portal/-/issues/38) - AWS Group Accounts - The creation is only available using the CLI since we do not have an account naming and user invitation/management UI created yet.
* `High` [#4](https://gitlab.com/hackystack/hackystack-portal/-/issues/4) - Local authentication form has not been implemented
* `Medium` [#5](https://gitlab.com/hackystack/hackystack-portal/-/issues/5) - GCP API has not been fully implemented due to GCP Resource Manager API limitations
* `Medium` [#6](https://gitlab.com/hackystack/hackystack-portal/-/issues/6) - Logging has only been partially implemented on `Auth` services
* `Medium` [#7](https://gitlab.com/hackystack/hackystack-portal/-/issues/7) - Logging has not been implemented on `Cloud` services
* `Low` [#8](https://gitlab.com/hackystack/hackystack-portal/-/issues/8) - Queued jobs for expiration have not be implementation

## Merge Requests (21)
* `backend` Add AWS root email dynamic generation capability - !85 - @jeffersonmartin
* `backend` Add Bugsnag API Key to .env.example - !88 - @jeffersonmartin
* `backend` Add Vendor/Aws/IamUserService createLoginProfile() method to provisioning of Cloud Accounts - !101 - @jeffersonmartin
* `backend` Add support for Bugsnag log provider - !87 - @jeffersonmartin
* `backend` Add support for dynamic tagging of cloud accounts - !84 - @jeffersonmartin
* `backend` Fix AWS API race condition and rate limits when importing AWS Organization Units - !89 - @jeffersonmartin
* `backend` Fix AWS IAM User password generation to enforce character type requirements - !105 - @jeffersonmartin
* `backend` Fix Cloud Realm root organization unit ID missing in database rows - !96 - @jeffersonmartin
* `backend` Fix Vendor/AWS/IAMUserService createLoginProfile() to remove typo in parameters - !102 - @jeffersonmartin
* `backend` Fix cloud account provisioning recursive background job to fail after number of attempts - !93 - @jeffersonmartin
* `backend` Fix config/hackystack-*.php.example files to be empty and move GitLab examples to `.example-gitlab` - !95 - @jeffersonmartin
* `backend` Update Cloud Account User background job to fix race condition and add envoy.blade.php example - !90 - @jeffersonmartin
* `backend` Update env examples with AWS root email variables - !86 - @jeffersonmartin
* `cli` Add Changelog generator CLI command - !106 - @jeffersonmartin
* `cli` Add cloud-account:* console commands - !97 - @jeffersonmartin
* `cli` Fix cloud-account-user-role:create to remove deprecated output after creation - !99 - @jeffersonmartin
* `cli` Fix cloud-account-user:create and AuthUser model to use correct namespace for CloudAccountUser model - !98 - @jeffersonmartin
* `cli` Update background job logs for cloud accounts - !91 - @jeffersonmartin
* `cli` Update cloud-account-* CLI command with improved outputs - !103 - @jeffersonmartin
* `other` Add DOCKERFILE for easy installation of HackyStack Portal - !94 - @jsandlin
* `user-ui` Update UI for creating and viewing group AWS accounts - !100 - @jeffersonmartin

## Commits (116)
* `backend` Add envoy.blade.php.example - 78de92cf - !90
* `backend` Add sleep functions to Console/Commands/Cloud/CloudRealmImportConfigFile - 740c6e2b - !89
* `backend` Fix Vendor/Aws/IamUserService deprecated comments in create() method - b68cd1a7 - !101
* `backend` Initial commit of empty config/hackystack-auth-groups.php.example - f745a086 - !95
* `backend` Initial commit of empty config/hackystack-auth-provider-fields.php.example - 15625f89 - !95
* `backend` Initial commit of empty config/hackystack-cloud-realms.php.example - 3bdf8e22 - !95
* `backend` Merge branch '12-add-aws-root-email-dynamic-generation-capability' of gitlab.com:hackystack/hackystack-portal into 12-add-aws-root-email-dynamic-generation-capability - e1a409e5 - !86
* `backend` Refactor Console/Commands/Cloud/CloudAccountCreateStatusRefresh to add conditional checks for state when provisioning Cloud Account Users - 9a36c5f2 - !93
* `backend` Rename config/hackystack-auth-groups.php.example to .example-gitlab - 0c976497 - !95
* `backend` Rename config/hackystack-auth-provider-fields.php.example to .example-gitlab - 9bc3d730 - !95
* `backend` Rename config/hackystack-cloud-realms.php.example to .example-gitlab - ec9d663f - !95
* `backend` Update .env.example to add AWS root email variables - 18cd3907 - !86
* `backend` Update .env.example to add BUGSNAG_API_KEY - 6b8ba76f - !88
* `backend` Update .gitignore to add /config/hackystack.php - d34916d0 - !90
* `backend` Update .gitignore to add envoy.blade.php - c0874267 - !90
* `backend` Update CloudAccountService to add AWS root email method with plus concatenation support - 7d5bfc72 - !85
* `backend` Update CloudAccountService to add AWS root email method with plus concatenation support - 7d5bfc72 - !86
* `backend` Update CloudAccountService to add conditional State when provisioning an account to catch instance vs delayed variations - bd3d3f0e - !90
* `backend` Update CloudAccountService to add generateTags method - 22473095 - !84
* `backend` Update CloudAccountService to refactor provision method with tags method - 0532f05a - !84
* `backend` Update CloudAccountUserService provision() method to invoke createLoginProfile() method after IAM user is created - 2e662a20 - !101
* `backend` Update CloudAccountUserService to refactor password generation using complexity requirements - c1186732 - !105
* `backend` Update CloudRealmService store method to add cloud_organization_unit_id_root after organization unit is created - b0360baf - !96
* `backend` Update Console/Commands/Cloud/CloudAccountCreateStatusRefresh to refactor cloud account user provisioning status conditional - 4efcbcfd - !90
* `backend` Update Console/Commands/HackystackInstall to add prompts for AWS root email variables - d1fde859 - !86
* `backend` Update Vendor/Aws/IamUserService to remove typo $id parameter from method. - d0103ab6 - !102
* `backend` Update composer dependencies to add hackzilla/password-generator package - fa6865fb - !105
* `backend` Update composer to add bugsnag package, update guzzle version dependency, and rename deprecated faker package - c28bd7b8 - !87
* `backend` Update config/app to add Bugsnag Service Provider - 8b321b0e - !87
* `backend` Update config/hackystack to add accounts.aws.tags and accounts.gcp.labels arrays - 416c5bfd - !84
* `backend` Update config/hackystack to add root_email nested array with method (future expansion), plus_prefix and plus_domain - 46c8c541 - !85
* `backend` Update config/hackystack to add root_email nested array with method (future expansion), plus_prefix and plus_domain - 46c8c541 - !86
* `backend` Update config/hackystack.php to add AWS IAM User Password Complexity Requirements - e916c501 - !105
* `backend` Update config/hackystack.php.example to add AWS IAM User Password Complexity Requirements - 37c5701b - !105
* `backend` Update config/hackystack.php.example to add accounts.aws.tags and accounts.gcp.labels arrays - 9c647d48 - !84
* `backend` Update config/logging to add bugsnag driver and add to stack channel - 3ef2e390 - !87
* `cli` Add cloud-account-role:create CLI command - bff22b3d - !97
* `cli` Add cloud-account-user-role:create CLI command - d7993a53 - !97
* `cli` Add cloud-account-user-role:delete CLI command - a0fcec87 - !97
* `cli` Add cloud-account-user:create CLI command - 52d282dc - !97
* `cli` Add cloud-account-user:delete CLI command - 541a6e7c - !97
* `cli` Add cloud-account-user:get CLI command - f1f48219 - !97
* `cli` Add cloud-account-user:list CLI command - ccddcf2b - !97
* `cli` Add coud-account-role:get CLI command - 5190f13c - !97
* `cli` Add developer:generate-changelog CLI command - be6bfc02 - !106
* `cli` Fix CloudAccountUserService to rename comment typos Organization Unit to IAM User - e77ef8e8 - !97
* `cli` Fix Models/Auth/AuthUser to add namespace to cloudAccountUsers() relationship - ea675e01 - !98
* `cli` Fix Models/Auth/AuthUser to add namespace to cloudAccounts() relationship - 3b129c31 - !98
* `cli` Fix Models/Cloud/CloudAccountUser typo with cloud_account_group_id in fillable array - db8c7ffc - !97
* `cli` Fix cloud-account-user-role:create CLI command to rename table header typo Group Slug to Cloud Accounts - e32a8385 - !103
* `cli` Fix cloud-account-user-role:create to remove the deprecated cloud-account-user-role:get output - 5f263a98 - !99
* `cli` Fix cloud-account-user:create CLI command to rename table header typo Group Slug to Cloud Accounts - 5dee522f - !103
* `cli` Update .env.example to add HackyStack Developer variables for GitLab API - cc9e24d2 - !106
* `cli` Update CloudAccountUserService `deprovision()` method to implement AWS IAM User Service deleteUser SDK - e533ab64 - !97
* `cli` Update Console/Commands/Cloud/CLoudAccountCreateStatusRefresh to refactor comment outputs to be log friendly - dfd5e63f - !91
* `cli` Update Console/Commands/Cloud/CloudAccountGet to add cloud account groups, users and user roles relationships - df4695c1 - !97
* `cli` Update Console/Commands/Cloud/CloudAccountGet to fix comments and condense relationship outputs - 527c0f5d - !97
* `cli` Update Models/Auth/AuthUser to add cloudAccountUsers relationship - d692c0dd - !97
* `cli` Update Models/Cloud/CloudAccount to add cloudAccountGroupsRoles relationship - 335b8d87 - !97
* `cli` Update Models/Cloud/CloudAccount to add cloudAccountUsersRoles relationship - 673df042 - !97
* `cli` Update Models/Cloud/CloudAccountRole to add cloudAccountGroups relationship - 29874800 - !97
* `cli` Update Models/Cloud/CloudAccountRole to add cloudAccountUsers relationship - 3db8c692 - !97
* `cli` Update Vendor/AWS/IAMUserService create() method to remove comment block references to recently refactored createLoginProfile method - 6f0677c4 - !97
* `cli` Update Vendor/AWS/IAMUserService create() method to return an array of API returned values - 91c716c0 - !97
* `cli` Update Vendor/AWS/IAMUserService createLoginProfile method to refactor returned array (breaking change) - 1b48c411 - !97
* `cli` Update Vendor/AWS/IAMUserService to implement createAccessKey() method - 85ff9e69 - !97
* `cli` Update Vendor/AWS/IAMUserService to implement delete() method - 4ea7a337 - !97
* `cli` Update Vendor/AWS/IAMUserService to implement get() method - c0b4a8d1 - !97
* `cli` Update `cloud-account-user:create` CLI command to move instructions for cloud-account-user-role:create to end of console output. - 3360c9f2 - !103
* `cli` Update app/Console/Kernel to add Commands/Developer namespace - 3fd18734 - !106
* `cli` Update app/Console/Kernel to add log output to scheduled commands - 21beb0a2 - !91
* `cli` Update cloud-account-role:create CLI command to move instructions for cloud-account-user-role:create to end of console output - 6dc16cd8 - !103
* `cli` Update cloud-account-user-role:create CLI command to filter list of CloudAccountUsers values to only include users that aren't associated with the CloudAccountRole yet. - d8903641 - !103
* `cli` Update cloud-account-user:create CLI command to add --with-password argument after user completion to show IAM password in console output. - 9727c085 - !103
* `cli` Update cloud-account-user:get CLI command to add `--with-password` argument - 35b49e56 - !97
* `cli` Update cloud-account-user:get CLI command to add all remaining database fields and relationships to console output - 2fab5b9d - !97
* `cli` Update cloud-account:create CLI command to add instructions above the slug about the naming conventions, include a list of sample account names that have been previously created (both individual and group). - 8365f767 - !103
* `cli` Update config/hackystack.php and hackystack.php.example with the GitLab API variables for developers - 9ba8bd7a - !106
* `cli` Update dev_env/mysql/Dockerfile - 43b177fd - !97
* `other` Merge branch 'jsandlin' into 13-create-dockerfile-for-easy-installation-of-hackystack-portal - 993838dd - !94
* `other` almostworks - 05f7f06b - !94
* `other` ci - 30dc6741 - !94
* `other` ci - 6fb45a6d - !94
* `other` comments - 0d9bb38e - !94
* `other` compose - 10171fe5 - !94
* `other` compose - 933ac36e - !94
* `other` dev_docker_test - 0241c814 - !94
* `other` dev_docker_test - 21ab142d - !94
* `other` dev_docker_test - c63c3e18 - !94
* `other` dev_docker_test - e5f2cdfb - !94
* `other` docker - 776a9289 - !94
* `other` docker - c0b95313 - !94
* `other` dockerdev - 4b6b318f - !94
* `other` dockerfile - b4c5f1f4 - !94
* `other` dockerize - 1be47671 - !94
* `other` env changes - 9975aae5 - !94
* `other` idea - 3f3097f5 - !94
* `other` move - 9dd655b0 - !94
* `other` move sercies - 85974d2c - !94
* `other` move sercies - a44623ae - !94
* `other` movetodockerdev - 60f84421 - !94
* `other` mysql - 47df7775 - !94
* `other` remove idea - 1f402c72 - !94
* `other` touchup - fb10a348 - !94
* `other` try custom image - feb8f71d - !94
* `other` works - 0f2f0e41 - !94
* `other` works - 219489d8 - !94
* `other` works - 71422635 - !94
* `other` works - a1e3a70b - !94
* `other` works - e588e2bb - !94
* `user-ui` Fix views/user/cloud/accounts/aws/show list of cloud account roles to add line breaks - ac100fba - !100
* `user-ui` Fix views/user/cloud/accounts/aws/show looped cloud account users variable that broke the IAM credential modal window - 01febe5f - !100
* `user-ui` Update views/user/cloud/index to add `group` badge on shared accounts in table - 32a88987 - !100
* `user-ui` Update views/user/cloud/index to adjust table column width for longer account names - 5c233b0c - !100
* `user-ui` Update views/user/cloud/index to rename Create Account button to Create Individual Account - 60797e27 - !100
* `user-ui` Update views/user/cloud/index to rename Create Group button (disabled) and alert message with CLI instructions - 3835e8a9 - !100
