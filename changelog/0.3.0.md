# 0.3.0
* **URL:** https://gitlab.com/hackystack/hackystack-portal/-/milestones/9
* **Release Date:** 2021-07-19

## Overview
This is the third release of HackyStack that implements GCP billing accounts, folder, project, and IAM user provisioning, as well as several enhancements to the CLI interface.

### New Features
* GCP Billing Accounts - You can associate a GCP billing account with a Cloud Billing Account, which is mapped to a Cloud Realm and inherited by all Cloud Accounts in that Cloud Account. Additional CLI commands for `cloud-billing-account:*` have been added for administrative configuration.
* GCP Folders - A new folder is provisioned for each Cloud Organization Unit in a `gcp` Cloud Provider.
* GCP Projects - A new project is provisioned for each Cloud Account in a `gcp` Cloud Provider.
* GCP IAM Users - An IAM policy is created for each Cloud Account User to provide permissions to a Google user with the Auth User email address.

### Resolved Limitations
* [#5](https://gitlab.com/hackystack/hackystack-portal/-/issues/5) - GCP API has not been fully implemented due to GCP Resource Manager API limitations

### Limitations
* `High` [#65](https://gitlab.com/hackystack/hackystack-portal/-/issues/65) - Automatically enable GCP service APIs after project is completed.
* `High` [#38](https://gitlab.com/hackystack/hackystack-portal/-/issues/38) - AWS Group Accounts - The creation is only available using the CLI since we do not have an account naming and user invitation/management UI created yet.
* `High` [#4](https://gitlab.com/hackystack/hackystack-portal/-/issues/4) - Local authentication form has not been implemented
* `Medium` [#6](https://gitlab.com/hackystack/hackystack-portal/-/issues/6) - Logging has only been partially implemented on `Auth` services
* `Medium` [#7](https://gitlab.com/hackystack/hackystack-portal/-/issues/7) - Logging has not been implemented on `Cloud` services
* `Low` [#8](https://gitlab.com/hackystack/hackystack-portal/-/issues/8) - Queued jobs for expiration have not be implementation

## Merge Requests (16)
* `backend` Add GCP billing account automation for new GCP Cloud Accounts - !119 - @jeffersonmartin
* `backend` Fix permissions issues with storage/keys/cloud_providers path - !114 - @jeffersonmartin
* `backend` Implement GCP/CloudResourceManagerFolderService with v3 API resources - !111 - @jeffersonmartin
* `backend` Implement GCP/CloudResourceManagerProjectService with v3 API resources - !113 - @jeffersonmartin
* `backend` Update CloudAccountUser to add provision_at and expires_at timestamps and flags - !116 - @jeffersonmartin
* `cli` Fix auth-user:list CLI command search arguments Column not found error - !126 - @jeffersonmartin
* `cli` Fix cloud-account-user:get CLI command to check if password is null - !121 - @jeffersonmartin
* `cli` Update Cloud CLI commands with improved verbose outputs - !124 - @jeffersonmartin
* `cli` Update auth-user:get CLI command to show cloudAccountUser relationships table - !129 - @jeffersonmartin
* `cli` Update auth-user:get CLI command to show provider_meta_data array table - !127 - @jeffersonmartin
* `cli` Update cloud-realm:import-config-file CLI command to add progress verbosity - !122 - @jeffersonmartin
* `docs` Update CHANGELOG to create file per release in changelog/ directory - !112 - @jeffersonmartin
* `docs` Update config/hackystack-auth-groups.php.example-gitlab with latest department names - !115 - @jeffersonmartin
* `docs` Update config/hackystack-cloud-realms.php.example-gitlab with department name updates - !120 - @jeffersonmartin
* `user-ui` Add Organization unit/folder and Account/Project ID for Cloud Account to UI - !128 - @jeffersonmartin
* `user-ui` Fix GCP IAM credentials modal conditional to check for Cloud Account provisioning status - !118 - @jeffersonmartin

## Commits (110)
* `backend` Add Services/V1/Vendor/Gcp/CloudResourceManagerFolderService get method - 114a6e23 - !111
* `backend` Add Vendor/Gcp/CloudResourceManagerIamUserRoleService - 620cd7a3 - !113
* `backend` Add Vendor/Gcp/CloudResourceManagerProjectService - 65365fe3 - !113
* `backend` Add cloud-account-user-role:scheduled-provisioning CLI command - a7c0bf65 - !113
* `backend` Add cloud-account-user:scheduled-provisioning CLI command - 5ee0dce8 - !113
* `backend` Add cloud-account:provision-gcp-billing-account CLI command - 9d01baa3 - !119
* `backend` Add cloud-billing-account:edit CLI command with capability to associate GCP billing account using API - 47a8a454 - !119
* `backend` Add migrations/add_api_meta_data_to_cloud_accounts_user_roles_table - 1de816f6 - !113
* `backend` Add migrations/add_expired_at_to_cloud_accounts_users_table - 24ca9903 - !116
* `backend` Add migrations/add_expires_at_to_cloud_accounts_users_table - 86bd7a6c - !116
* `backend` Add migrations/add_flag_expired_to_cloud_accounts_users_table - b0477707 - !116
* `backend` Add migrations/add_provision_at_to_cloud_accounts_users_roles_table - c5639ab4 - !113
* `backend` Add migrations/add_provision_at_to_cloud_accounts_users_table - 1a529364 - !116
* `backend` Add views/user/cloud/accounts/gcp/show - 247edbd4 - !113
* `backend` Fix CloudAccountService to add empty suffix for string limited project names instead of ... - 62c8e941 - !113
* `backend` Fix CloudAccountUserRoleService provision method GCP project ID to use api_meta_data['name'] field - a5120f18 - !113
* `backend` Fix CloudAccountUserService provision method to uncomment GCP flag_provisioned and state values for user creation dependency - 90a26c3d - !113
* `backend` Fix CloudProviderService to change GCP default_roles Owner to owner - a9e353a6 - !113
* `backend` Fix Controllers/User/CloudAccountController store method to remove erroneous cloud_billing_account_id variable - bae49368 - !119
* `backend` Fix Services/V1/Cloud/CloudAccountService api_meta_data array typo with count function - c6940bca - !119
* `backend` Fix Services/V1/Cloud/CloudBillingAccountService to remove json_encode from api_meta_data to fix database storage bug - 03f7a438 - !119
* `backend` Fix Services/V1/Cloud/CloudOrganizationUnitService api_meta_data array typo with count function - 62d0db41 - !119
* `backend` Fix Services/V1/Cloud/CloudOrganizationUnitService provisionCloudProviderOrganizationUnit method to fix variable typo with $parent_folder_name - a0df58f9 - !111
* `backend` Fix Services/V1/Cloud/CloudOrganizationUnitService to fix typo with git_meta_data - f8ce64ec - !111
* `backend` Fix Services/V1/Cloud/CloudProviderService api_meta_data array typo with count function - c0b9e0ad - !119
* `backend` Fix Services/v1/Cloud/CloudProviderService syncCloudProviderOrganization method comment typo - b57a90b4 - !111
* `backend` Fix User/CloudAccountController to change GCP cloud_account_role_name from Owner to owner - 7098e52c - !113
* `backend` Fix cloud-account-user:get CLI command to add conditionals for AWS and GCP metadata differences - 48f2fd97 - !119
* `backend` Fix cloud-billing-account:create CLI command to fix realm typo in --help documentation - f926e5b0 - !119
* `backend` Fix cloud-provider:credential CLI command to update chown and chgrp settings for storage_path(keys/cloud_providers) and JSON key file - 52699a1d - !114
* `backend` Fix cloud-provider:credential CLI command to update parent_org_unit prompt typo with existing folder selection - 5304239d - !119
* `backend` Fix migrations/add_api_meta_data_to_cloud_account_users_roles_table to fix table plural name - 95ec094c - !113
* `backend` Fix views/user/cloud/accounts/create to list of cloud providers with AWS MasterAccountID error and refactor conditional logic for GCP - 6a0fc67d - !113
* `backend` Merge branch 'main' into '57-add-gcp-billing-account-automation-for-new-gcp-cloud-accounts' - a9f6253b - !119
* `backend` Update CloudAccountController to add conditional cloud account slug convention for GCP projects - 1f16159e - !113
* `backend` Update CloudAccountController to add provision_at with 90 second delay for user accounts to await GCP project provisioning completion - fdc01493 - !113
* `backend` Update CloudAccountService deprovision method to implement GCP cloud provider type deprovisioning - a7dcbbfa - !113
* `backend` Update CloudAccountService provision method to add docblock comment - ade4b54c - !113
* `backend` Update CloudAccountService provision method to add missing false return with AWS provisioning-error - 9ccaaf23 - !113
* `backend` Update CloudAccountService provision method to fix typo with project service initialization - 7740539e - !113
* `backend` Update CloudAccountService provision method to implement GCP cloud provider type provisioning - 2f333e21 - !113
* `backend` Update CloudAccountService provision method to set labels array to null if empty - ee871e36 - !113
* `backend` Update CloudAccountService provision method to validate returned response state - f24c1df9 - !113
* `backend` Update CloudAccountUserRoleService to add provision_at column and conditionals - fcd8da68 - !113
* `backend` Update CloudAccountUserRoleService to fix api_meta_data conditional for GCP - 3f227e09 - !113
* `backend` Update CloudAccountUserService provision method to add comments and return true for GCP users (that use IAM roles instead) - ecb78cc2 - !113
* `backend` Update CloudOrganizationUnitService deprovision method to update api_meta_data value instead of setting to null - a8fc4731 - !111
* `backend` Update CloudOrganizationUnitService to add docblock comments - f6f77e7d - !111
* `backend` Update Console/Kernel scheduled commands to add cloud-account-user-role:scheduled-provisioning - b0514873 - !113
* `backend` Update Console/Kernel scheduled commands to add cloud-account-user:scheduled-provisioning - be3ba806 - !113
* `backend` Update Models/Cloud/CloudAccountUser to add provision_at and expires_at columns to casts and validation rules - 2815e954 - !116
* `backend` Update Models/Cloud/CloudAccountUser to add scopes for provisioned and expired conditionals - 1c8300ec - !116
* `backend` Update Models/Cloud/CloudAccountUser to add setFlagExpiredAttribute setter - f88f2f8c - !116
* `backend` Update Models/Cloud/CloudAccountUserRole to add api_meta_data column - bb3313e8 - !113
* `backend` Update Models/Cloud/CloudAccountUserRole to add provision_at column - 53534c68 - !113
* `backend` Update Services/V1/Cloud/CloudAccountService to add provisionBillingAccount method for GCP - 98b7b803 - !119
* `backend` Update Services/V1/Cloud/CloudOrganizationUnitService delete method to implement child relationship validation - e5cd272e - !111
* `backend` Update Services/V1/Cloud/CloudOrganizationUnitService to add TODO comments for future renaming API calls - 9eb94038 - !111
* `backend` Update Services/V1/Cloud/CloudOrganizationUnitService to rename display_name parameter to folder_name (internal only breaking change) - d71d92db - !111
* `backend` Update Services/V1/Vendor/CloudResourceManagerFolderService to rename display_name parameter to folder_name (internal only breaking change) - 9ee9a3df - !111
* `backend` Update Services/V1/Vendor/Gcp/BaseService to update setGoogleApiClient credentials based on JSON file format changes - 265799c4 - !111
* `backend` Update Services/V1/Vendor/Gcp/CloudBillingOrganizationService get method to return an array instead of an object (breaking change) - b30b24a6 - !119
* `backend` Update Services/V1/Vendor/Gcp/CloudBillingOrganizationService list method to return an array instead of an object (breaking change) - 525c3cd4 - !119
* `backend` Update Services/V1/Vendor/Gcp/CloudBillingOrganizationService to refactor comments using updated standards - 8bfce6de - !119
* `backend` Update Services/V1/Vendor/Gcp/CloudBillingProjectService update method to add comments for expected return array values - 9cad7d6c - !119
* `backend` Update Services/V1/Vendor/Gcp/CloudResourceManagerFolderService with refactored provision and deprovision methods based on v3 API testing - ab8c1bd3 - !111
* `backend` Update Services/v1/Cloud/CloudProviderService syncCloudProviderOrganization method to implement GCP folder API get call - 8a44729c - !111
* `backend` Update User/CloudAccountController to shorten account name for GCP due to 30-char limit - 80090cff - !113
* `backend` Update Vendor/Gcp/CloudBillingProjectService to add get and update method - 4ebe1dc0 - !119
* `backend` Update Vendor/Gcp/CloudBillingProjectService to reformat documentation links and sample output comments - 0cc07540 - !119
* `backend` Update Vendor/Gcp/CloudBillingProjectService to remove unused list() method - 42f35bbd - !119
* `backend` Update Vendor/Gcp/CloudResourceManagerIamUserRoleService with refactored create method with test debugging - 08af3bbc - !113
* `backend` Update Vendor/Gcp/CloudResourceManagerProjectService deprovision method to add exception handling - 3c40169e - !119
* `backend` Update Vendor/Gcp/CloudResourceManagerProjectService provision method to update comment with expected API response - 328ac3cc - !113
* `backend` Update Vendor/Gcp/CloudResourceManagerProjectService to reformat documentation links and sample output comments - d4c3bf86 - !119
* `backend` Update Vendor/Gcp/CloudResourceProjectManagerService get method to move return array to end of method - 9db19ea8 - !119
* `backend` Update cloud-billing-account:create CLI command to add GCP billing account API lookup and association logic - 755e22b7 - !119
* `backend` Update cloud-billing-account:get CLI command to abbreviate authTenant and cloudProvider parent relationships in console output - f7a3c660 - !119
* `backend` Update cloud-billing-account:get CLI command to add API meta data table with GCP billing account information - 008c1a36 - !119
* `backend` Update cloud-provider:credential GCP storage_path folder to use config and env variables for chown user and group - 22665e73 - !119
* `backend` Updated CloudAccountUserRoleService provision method to implement GCP provisioning - beb70b84 - !113
* `backend` WIP Services/V1/Vendor/GCPCloudBillingOrganizationService - a7feed3b - !119
* `backend` WIP Services/V1/Vendor/Gcp/CloudBillingProjectService - b340b1c4 - !119
* `cli` Fix auth-user:list CLI command to change --search keywords to use full_name, email, and provider_meta_data fields - 3ac31c82 - !126
* `cli` Fix cloud-account-user:create CLI command table output key names for relationships - 1d74c417 - !124
* `cli` Fix cloud-account-user:get CLI command to check if password is null - e0d16755 - !121
* `cli` Update app/Console/Commands/Cloud/CloudRealmImportConfigFile to add console output lines after each record is created - 688735bb - !122
* `cli` Update auth-user:get CLI command to add Auth Provider meta data table - 82d5e0af - !127
* `cli` Update auth-user:get CLI command to add Cloud Account Users relationship table - 6ceae8e3 - !129
* `cli` Update cloud-account-user:list CLI command to add cloud_account_users_roles count and provisioned_at timestamp to table - b5620d22 - !124
* `cli` Update cloud-account-user:list CLI command to add cloud_provider search filter - 9416c431 - !124
* `cli` Update cloud-organization-unit:list CLI command to add provisioned flag column to table - e892fe44 - !124
* `cli` Update cloud-provider:get CLI command to add API meta data to output - e9104bf8 - !124
* `cli` Update cloud-provider:get CLI command to add defaultRoles to output - 46327dc8 - !124
* `cli` Update cloud-provider:get CLI command to add git_meta_data[web_url] column to output - 7ec5b274 - !124
* `cli` Update cloud-provider:get CLI command to add parent_org_unit column to output - fb1704f3 - !124
* `cli` Update cloud-realm:get CLI command to add API meta data to output - 287d5efe - !124
* `cli` Update cloud-realm:get CLI command to remove verbose relationship tables - 78ba4f3c - !124
* `docs` Add changelog/0.1.0-beta - 6830182a - !112
* `docs` Add changelog/0.2.0 - 90ab4539 - !112
* `docs` Update CHANGELOG.md to remove static releases and redirect to changelog directory - 416c114e - !112
* `docs` Update config/filestystems to add changelog disk configuration - 0e4e9a01 - !112
* `docs` Update config/hackystack-auth-groups.php.example-gitlab with updated GitLab department names - fc6d5e83 - !115
* `docs` Update config/hackystack-cloud-realms.php.example-gitlab with department name updates - e5a83ddb - !120
* `docs` Update developer:generate-changelog CLI command to generate a new file for a release instead of console output - d8ceab95 - !112
* `user-ui` Fix cloud-account-role:create description grammar typo - bb5af8fe - !128
* `user-ui` Fix views/user/cloud/accounts/aws/show to update conditional for AWS Account ID and email - 996edcf3 - !128
* `user-ui` Update views/user/cloud/accounts/gcp/_modals/iam-credentials to fix array_key_exists from Id to projectId - a61cb0da - !118
* `user-ui` Update views/user/cloud/accounts/gcp/show to add conditional for showing GCP project ID - b68f0b4b - !128
* `user-ui` Update views/user/cloud/index to add organization unit and account/project ID for each Cloud Account - f216d753 - !128
